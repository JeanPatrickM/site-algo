<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $aT[0] = [21, 56, 8, 654, 0, 684, 34, 67];
    $aT[1] = [62, 689, 76, 8, 7, 5, 55, 557];
    $aT[2] = [85, 554, 22, 867, 83, 837, 785, 3];
    $aT[3] = [35, 87, 678, 23, 58, 0, 85, 36];
    $aT[4] = [786, 38, 783, 38, 95, 89, 87, 287];
    $aT[5] = [76, 976, 876, 88, 676, 76, 97, 19];
    $aT[6] = [29, 15, 321, 932, 32, 15, 69, 972];
    $aT[7] = [1, 152, 5, 22, 5432, 684, 178, 71];
    $aT[8] = [37, 875, 85, 76, 86, 48, 21, 782];
    $aT[9] = [35, 414, 328, 811, 857, 854, 2, 157];
    $aT[10] = [383, 586, 18, 188, 81, 851, 159, 6];
    $aT[11] = [196, 62, 6, 52, 57, 5, 4, 3];
    
    for ($iCount = 0; $iCount < 12; $iCount++) {
        for ($jCount = 0; $jCount < 8; $jCount++) {
            if (($iCount === 0 && $jCount === 0) || $iValueMax < $aT[$iCount][$jCount]) {
                $iValueMax = $aT[$iCount][$jCount];
                $iIndex_1_Max = $iCount;
                $iIndex_2_Max = $jCount;
            }
        }
    }

    $sAnswer = 
        '<span style="color: grey;">Voici la plus grande valeur contenu dans ce tableau aT :<br><br>' .
        $iValueMax .
        "<br><br>Et elle se trouve aux cordonnées :<br><br> aT[" .
        $iIndex_1_Max .
        "][" .
        $iIndex_2_Max .
        "]</span>";
}

require "exo_6.html";

?>

<!-- = '<span style="color: grey;"> -->     <!-- </span>' -->