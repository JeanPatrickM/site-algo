<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $sArray_1 = "";
    $sArray_2 = "";
    
    for ($iCount = 0; $iCount < 6; $iCount++) {
        $aArray[$iCount] = [];

        $sArray_2 = "";
        for ($jCount = 0; $jCount < 13; $jCount++) {
            $aArray[$iCount][$jCount] = 0;

            if ($jCount < 12) {
                $sArray_2 .= $aArray[$iCount][$jCount] . ", ";
            } else {
                $sArray_2 .= $aArray[$iCount][$jCount] . "]<br>";
            }
        }

        $sArray_1 .= "aArray[" . $iCount . "] =  [" . $sArray_2;
    }
        
    $sAnswer = 
        '<span style="color: grey;">Voici le tableau à 2 dimensions, de 6 et 13, une fois rempli de 0 pour chaque index :<br><br>' .
        $sArray_1 .
        "</span>";
}

require "exo_7.html";

?>

<!-- = '<span style="color: grey;"> -->     <!-- </span>' -->