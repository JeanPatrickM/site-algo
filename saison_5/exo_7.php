<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumber = (int)$_POST['iNumber'];
  $iFactorial = 1;

  for ($iCount = $iNumber - 1; $iCount >= 0; $iCount--) {
    $iFactorial = $iFactorial * ($iNumber - $iCount);
  }
  
  $sAnswer = '<span style="color: grey;">' . $iNumber . '! est égale à ' . $iFactorial . '</span>';
}

require "exo_7.html";

?>
