<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumber = (int)$_POST['iNumber'];
  $iSum = (int)0;

  for ($i = $iNumber - 1; $i >= 0; $i--) {
      $iSubtraction = $iNumber - $i;
      $iSum += $iSubtraction;
  }
  
  $sAnswer = '<span style="color: grey;">La somme des entiers jusqu\'à ' . $iNumber . ' est égale à ' . $iSum . '</span>';
}

require "exo_6.html";

?>
