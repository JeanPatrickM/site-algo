<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumberStart = (int)$_POST['iNumberStart'];
  $sResult = "";

  for ($i = 0; $i < 10; $i++) {
    $iNumberStart++;

    if ($i < 9) {
      $sResult = $sResult . $iNumberStart . ', ';
    } else {
      $sResult = $sResult . $iNumberStart . '. ';
    }
  }
  
  $sAnswer = '<span style="color: grey;">Les 10 nombres suivants sont : ' . $sResult . '</span>';
}

require "exo_4.html";

?>
