<?php

$sAnswer = "";
$sLabel = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumberUser = (int)$_POST['iNumberUser'];
  $iItemNumber = (int)$_POST['iItemNumber'];
  $iLargestNumber = (int)$_POST['iLargestNumber'];
  $iPlaceLargestNumber = (int)$_POST['iPlaceLargestNumber'];
  
  if ($iItemNumber < 20) {
    if ($iItemNumber === 1 || $iLargestNumber < $iNumberUser) {
      $iLargestNumber = $iNumberUser;
      $iPlaceLargestNumber = $iItemNumber;
    }
    $sAnswer = '<span style="color: grey;">Votre nombre N°' . $iItemNumber . ' est le ' . $iNumberUser;
    $sLabel = '<span style="color: grey;">Veuillez maintenant entrer votre nombre N°' . ($iItemNumber + 1) . ' :';
  } else {
    if ($iLargestNumber < $iNumberUser) {
      $iLargestNumber = $iNumberUser;
      $iPlaceLargestNumber = $iItemNumber;
    }
    $sAnswer = '<span style="color: grey;">Votre nombre N°' . $iItemNumber . ' est le ' . $iNumberUser . '<br><br>Le plus grand nombre que vous avez entré est le ' . $iLargestNumber . '<br><br>Et c\'était le nombre N°' . $iPlaceLargestNumber . ' !';
    $sLabel = '<span style="color: grey;">Vous avez entré vos ' . $iItemNumber . ' nombres, regardez quel est le plus grand, à côté.';
  }

  $iItemNumber++;
} else {
  $iItemNumber = 1;
  $iLargestNumber = 0;
  $iPlaceLargestNumber = 0;
}

require "exo_8.html";

?>
