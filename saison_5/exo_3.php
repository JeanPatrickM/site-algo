<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumberStart = (int)$_POST['iNumberStart'];
  $sResult = "";

  $i = 0;

  while ($i < 10) {
    $iNumberStart++;

    if ($i < 9) {
      $sResult = $sResult . $iNumberStart . ', ';
    } else {
      $sResult = $sResult . $iNumberStart . '. ';
    }

    $i++;
  }
  
  $sAnswer = '<span style="color: grey;">Les 10 nombres suivants sont : ' . $sResult . '</span>';
}

require "exo_3.html";

?>
