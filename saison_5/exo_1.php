<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumber = (int)$_POST['iNumber'];

  if ($iNumber < 1 || $iNumber > 3) {
    $result = '<span style="color: grey;">Saisie erronée. Veuillez ressaisir un nombre qui soit bien compris entre 1 et 3, svp.</span>';
  } else {
    $result = '<span style="color: grey;">Bravo, vous avez réussi !</span>';
  }
}

require "exo_1.html";

?>
