<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumber = (int)$_POST['iNumber'];
  $sResult = "";

  for ($i = 1; $i <= 10; $i++) {
    $iMultiplication = $iNumber * $i;
    $sResult = $sResult . '&nbsp;&nbsp;&nbsp;&nbsp;' .
      $iNumber . ' x ' . $i . ' = ' . $iMultiplication . '<br>';
  }
  
  $sAnswer = '<span style="color: grey;">Table de ' . $iNumber . ' :<br><br>' . $sResult . '</span>';
}

require "exo_5.html";

?>
