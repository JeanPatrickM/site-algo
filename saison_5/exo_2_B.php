<?php

$result = "";

// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumberUser = (int)$_POST['iNumberUser'];
  $iNumberChancePhp = (int)$_POST['iNumberChancePhp'];
  $iNumberAttemptPhp = (int)$_POST['iNumberAttemptPhp'];
  
  if ($iNumberChancePhp === 0) {
  $iNumberChancePhp = random_int(1, 100);
  }
  
  $iNumberAttemptPhp++;
  
  if ($iNumberUser !== $iNumberChancePhp) {
    if ($iNumberUser < $iNumberChancePhp) {
      $result = '<span style="color: grey;">Retentez votre chance en saisissant un nombre plus grand.</span>';
    } else {
      $result = '<span style="color: grey;">Retentez votre chance en saisissant un nombre plus petit.</span>';
    }
  } else {
    $result = '<span style="color: grey;">Bravo, vous avez réussi en ' . $iNumberAttemptPhp . ' coup(s) !</span>';
  }
} else {
  $iNumberChancePhp = 0;
  $iNumberAttemptPhp = 0;
}

require "exo_2_B.html";

?>
