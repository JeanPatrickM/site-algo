<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iHorses = (int)$_POST['iHorses'];
  $iPlayedHorses = (int)$_POST['iPlayedHorses'];
  $iX = 1;
  $iFactorialPlayedHorses = 1;

  if ($iHorses >= $iPlayedHorses) {
    for ($iCount = $iPlayedHorses - 1; $iCount >= 0; $iCount--) {
      $iX = $iX * ($iHorses - $iCount);
      $iFactorialPlayedHorses = $iFactorialPlayedHorses * ($iPlayedHorses - $iCount);
    }

    $iY = $iX / $iFactorialPlayedHorses;
    $sAnswer = '<span style="color: grey;">Dans l\'ordre, vous avez 1 chance sur ' . $iX . ' de gagner. <br><br>Dans le désordre, vous avez 1 chance sur ' . $iY . ' de gagner.</span>';
  } else {
    $sAnswer = '<span style="color: grey;">Votre saisie est incorrect, vous ne pouvez pas jouer plus de chevaux que le nombre qui participe. <br><br>Veuillez indiquer des valeurs correctes, svp.</span>';
  }
}

require "exo_11.html";

?>
