<?php

$sAnswer = "";
$sLabel = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumberUser = (int)$_POST['iNumberUser'];
  $iItemNumber = (int)$_POST['iItemNumber'];
  $iLargestNumber = (int)$_POST['iLargestNumber'];
  $iPlaceLargestNumber = (int)$_POST['iPlaceLargestNumber'];
  
  if ($iNumberUser !== 0) {
    if ($iItemNumber === 1 || $iLargestNumber < $iNumberUser) {
      $iLargestNumber = $iNumberUser;
      $iPlaceLargestNumber = $iItemNumber;
    }
    $sAnswer = '<span style="color: grey;">Votre nombre N°' . $iItemNumber . ' est le ' . $iNumberUser . '</span>';
    $sLabel = '<span style="color: grey;">Veuillez maintenant entrer votre nombre N°' . ($iItemNumber + 1) . ' :</span>';

    $iItemNumber++;
  } else {
    if ($iLargestNumber < 0) {
      $iLargestNumber = 0;
      $iPlaceLargestNumber = $iItemNumber;
    }
    $sAnswer = '<span style="color: grey;">Votre nombre N°' . $iItemNumber . ' est le 0 <br><br> Le plus grand nombre que vous avez entré est le ' . $iLargestNumber . '<br><br>Et c\'était le nombre N°' . $iPlaceLargestNumber . ' !</span>';
    $sLabel = '<span style="color: grey;">Vous avez entré un 0, c\'est donc terminé, regardez à côté quel est le plus grand nombre entré.</span>';
  }
} else {
  $iItemNumber = 1;
  $iLargestNumber = 0;
  $iPlaceLargestNumber = 0;
}

require "exo_9.html";

?>
