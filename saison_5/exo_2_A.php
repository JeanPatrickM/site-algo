<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNumber = (int)$_POST['iNumber'];

  if ($iNumber < 10 || $iNumber > 20) {
    if ($iNumber < 10) {
      $result = '<span style="color: grey;">Veuillez ressaisir un nombre plus grand, svp.</span>';
    } else {
      $result = '<span style="color: grey;">Veuillez ressaisir un nombre plus petit, svp.</span>';
    }
  } else {
    $result = '<span style="color: grey;">Bravo, vous avez réussi !</span>';
  }
}

require "exo_2_A.html";

?>
