<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iDay = (int)$_POST['iDay'];
  $iMonth = (int)$_POST['iMonth'];
  $iYear = (int)$_POST['iYear'];
  $b31 = $iDay === 31 && ($iMonth === 1 || $iMonth === 3 || $iMonth === 5 || $iMonth === 7 || $iMonth === 8 || $iMonth === 10 || $iMonth === 12);
  $bBissextile = $iYear % 400 === 0 || ($iYear % 4 === 0 && $iYear % 100 !== 0);
  $b28 = $iDay >= 1 && $iDay <= 28 && $iMonth === 2;
  $b29 = $iDay === 29 && $iMonth === 2 && $bBissextile;
  $bAllOtherDates = $iDay >= 1 && $iDay <= 30 && $iMonth >= 1 && $iMonth <= 12 && $iMonth !== 2;


  if ($b28 || $b29 || $b31 || $bAllOtherDates) {
    $result = '<span style="color: grey;">La date est valide !</span>';
  } else {
    $result = '<span style="color: grey;">La date est invalide !</span>';
  }
}

require "exo_8.html";

?>
