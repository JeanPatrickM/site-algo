<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $iAge = (int)$_POST['iAge'];
    $iYearLicence = (int)$_POST['iYearLicence'];
    $iNbCrash = (int)$_POST['iNbCrash'];
    $iSeniorityCustomer = (int)$_POST['iSeniorityCustomer'];
    $bYoungNovice = $iAge < 25 && $iYearLicence < 2;
    $bMiddleClass = ($iAge < 25 && $iYearLicence >= 2) || ($iAge >= 25 && $iYearLicence < 2);
    $bOldExpert = $iAge >= 25 && $iYearLicence >= 2;
    $bNoviceDriver =
        ($bYoungNovice && $iNbCrash === 0) ||
        ($bMiddleClass && $iNbCrash === 1) ||
        ($bOldExpert && $iNbCrash === 2);
    $bMediumDriver = ($bMiddleClass && $iNbCrash === 0) || ($bOldExpert && $iNbCrash === 1);
    $bExpertDriver = $bOldExpert && $iNbCrash === 0;
    $bSeniorityCustomer = $iSeniorityCustomer >= 5;

    if ($bExpertDriver && $bSeniorityCustomer) {
        $result = '<span style="color: grey;">Vous bénéficiez du Tarif Bleu !</span>';
    } else if ($bExpertDriver || ($bMediumDriver && $bSeniorityCustomer)) {
        $result = '<span style="color: grey;">Vous bénéficiez du Tarif Vert !</span>';
    } else if ($bMediumDriver || ($bNoviceDriver && $bSeniorityCustomer)) {
        $result = '<span style="color: grey;">Vous bénéficiez du Tarif Orange !</span>';
    } else if ($bNoviceDriver) {
        $result = '<span style="color: grey;">Vous bénéficiez du Tarif Rouge !</span>';
    } else {
        $result = '<span style="color: grey;">Au vu de votre situation, nous ne pouvons pas vous assurer.</span>';
    }
}

require "exo_7.html";

?>
