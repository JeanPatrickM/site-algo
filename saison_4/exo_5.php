<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iAge = (int)$_POST['iAge'];
  $sSexe = $_POST['sSexe'];

  if ($iAge >= 20 && ($sSexe === "homme" || $sSexe === "Homme")) {
    $result = '<span style="color: grey;">Monsieur, vous êtes imposable.</span>';
  } else if ($iAge >= 18 && $iAge <= 35 && ($sSexe === "femme" || $sSexe === "Femme")) {
    $result = '<span style="color: grey;">Madame, vous êtes imposable.</span>';
  } else {
    $result = '<span style="color: grey;">Selon vos renseignements, vous n\'êtes pas imposable.</span>';
  }
}

require "exo_5.html";

?>
