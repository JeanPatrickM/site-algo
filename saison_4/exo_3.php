<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iHour = (int)$_POST['iHour'];
  $iMinute = (int)$_POST['iMinute'];
  $iSecond = (int)$_POST['iSecond'];

// Calcul de la nouvelle heure
  $iSecond++;
  if ($iSecond >= 100) {
    $iMinute++;
    $iSecond = $iSecond - 100;
    if ($iMinute >= 60) {
      $iHour++;
      $iMinute = $iMinute - 60;
      if ($iHour >= 24) {
        $iHour = $iHour - 24;
      }
		}
	}

// Affichage de la nouvelle heure
  if ($iHour === 12 && $iMinute === 0 && $iSecond === 0) {
    $result = '<span style="color: grey;">Dans une seconde, il sera midi.</span>';
  } else if ($iHour === 0 && $iMinute === 0 && $iSecond === 0) {
    $result = '<span style="color: grey;">Dans une seconde, il sera minuit.</span>';
  } else {
    if ($iMinute < 10 && $iSecond < 10) {
        $result = '<span style="color: grey;">Dans une seconde, il sera ' . $iHour . ' H 0' . $iMinute . ' Min et 0' . $iSecond . ' Sec.</span>';
    } else if ($iMinute < 10 && $iSecond >= 10) {
        $result = '<span style="color: grey;">Dans une seconde, il sera ' . $iHour . ' H 0' . $iMinute . ' Min et ' . $iSecond . ' Sec.</span>';
    } else if ($iMinute >= 10 && $iSecond < 10) {
        $result = '<span style="color: grey;">Dans une seconde, il sera ' . $iHour . ' H ' . $iMinute . ' Min et 0' . $iSecond . ' Sec.</span>';
    } else {
        $result = '<span style="color: grey;">Dans une seconde, il sera ' . $iHour . ' H ' . $iMinute . ' Min et ' . $iSecond . ' Sec.</span>';
    }
  }
}

require "exo_3.html";

?>
