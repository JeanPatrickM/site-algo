<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNbPhotocopy = (int)$_POST['iNbPhotocopy'];

  if ($iNbPhotocopy <= 10) {
      $iInvoice = $iNbPhotocopy * 0.1;
  } else if ($iNbPhotocopy > 10 && $iNbPhotocopy <= 30) {
      $iInvoice = 10 * 0.1 + ($iNbPhotocopy - 10) * 0.09;
  } else {
      $iInvoice = 10 * 0.1 + 20 * 0.09 + ($iNbPhotocopy - 30) * 0.08;
  }
  $result = '<span style="color: grey;">Votre facture s\'élève à ' . $iInvoice . '€</span>';
}

require "exo_4.html";

?>
