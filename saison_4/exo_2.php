<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
	$iHour = (int)$_POST['iHour'];
	$iMinute = (int)$_POST['iMinute'];

// Calcul de la nouvelle heure
	$iMinute++;
	if ($iMinute >= 60) {
		$iHour++;
		$iMinute = $iMinute - 60;
		if ($iHour >= 24) {
		$iHour = $iHour - 24;
		}
	}

// Affichage de la nouvelle l'heure
  	if ($iHour === 12 && $iMinute === 0) {
     	$result = '<span style="color: grey;">Dans une minute, il sera midi.</span>';
  	} else if ($iHour === 0 && $iMinute === 0) {
    	$result = '<span style="color: grey;">Dans une minute, il sera minuit.</span>';
  	} else {
    	if ($iMinute < 10) {
      		$result = '<span style="color: grey;">Dans une minute, il sera ' . $iHour . ' H 0' . $iMinute . '.</span>';
    	} else {
      		$result = '<span style="color: grey;">Dans une minute, il sera ' . $iHour . ' H ' . $iMinute . '.</span>';
		}
	}
}

require "exo_2.html";

?>
