<?php

$result = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iResultCandidate1 = (int)$_POST['iResultCandidate1'];
  $iResultCandidate2 = (int)$_POST['iResultCandidate2'];
  $iResultCandidate3 = (int)$_POST['iResultCandidate3'];
  $iResultCandidate4 = (int)$_POST['iResultCandidate4'];
  $bSecondRound = $iResultCandidate1 >= 12.5 && $iResultCandidate1 <= 50;
  $bTheFirst =
    $iResultCandidate1 > $iResultCandidate2 &&
    $iResultCandidate1 > $iResultCandidate3 &&
    $iResultCandidate1 > $iResultCandidate4;
  $bNotOtherAbsoluteMajority =
    $iResultCandidate2 <= 50 &&
    $iResultCandidate3 <= 50 &&
    $iResultCandidate4 <= 50;

  if ($iResultCandidate1 > 50) {
    $result = '<span style="color: grey;">Le Candidat N°1 est élu dès le 1er tour !</span>';
  } else if ($bSecondRound && $bTheFirst) {
    $result = '<span style="color: grey;">Le Candidat N°1 est en ballotage favorable pour le 2nd tour !</span>';
  } else if ($bSecondRound && !$bTheFirst && $bNotOtherAbsoluteMajority) {
    $result = '<span style="color: grey;">Le Candidat N°1 est en ballotage défavorable pour le 2nd tour !</span>';
  } else {
    $result = '<span style="color: grey;">Le Candidat N°1 est battu dès le 1er tour !</span>';
  }
}

require "exo_6.html";

?>
