<?php

$sAnswer = "";
$sLabel1 = "";
$sLabel2 = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iPriceArticle = (int)$_POST['iPriceArticle'];
  $iCash = (int)$_POST['iCash'];
  $iItemArticle = (int)$_POST['iItemArticle'];
  $iTotalPrice = (int)$_POST['iTotalPrice'];
  $sChange = "";
  
  if ($iCash < $iTotalPrice && $iCash !== 0 && $iPriceArticle === 0) {
          $sAnswer = '<span style="color: grey;">Désolé, vous n\'avez pas donné assez, veuillez indiquer une somme d\'argent supérieure ou égale à ' . 
                                                 $iTotalPrice . ' Euros, au même endroit.</span>';
      } else if ($iCash === $iTotalPrice && $iPriceArticle === 0) {
          $sLabel2 = '<span style="color: grey;">Votre règlement a bien été effectué.</span>';
          $sAnswer = '<span style="color: grey;">Merci, nous vous souhaitons une bonne journée !</span>';
      } else if ($iCash > $iTotalPrice && $iPriceArticle === 0) {
          $sLabel2 = '<span style="color: grey;">Votre règlement a bien été effectué, nous allons vous rendre votre monnaie, à côté.</span>';
          $iChange = $iCash - $iTotalPrice;

          while ($iChange >= 10) {
              $iChange -= 10;
              $sChange = $sChange . "10 Euros ";
          }

          if ($iChange >= 5) {
              $iChange -= 5;
              $sChange = $sChange . "5 Euros ";
          }

          while ($iChange !== 0) {
              $iChange--;
              $sChange = $sChange . "1 Euros ";
          }

          $sAnswer = '<span style="color: grey;">Merci, voilà votre monnaie concernant votre facture de ' . $iTotalPrice . ' Euros : <br><br>' . 
                                                 $sChange . 
                                                 '<br><br>Nous vous souhaitons une bonne journée !</span>';
      } else if ($iPriceArticle !== 0) {
          $iTotalPrice += $iPriceArticle;
          
          $sAnswer = '<span style="color: grey;">Votre article N°' . $iItemArticle . ' coûte ' . $iPriceArticle . ' Euros.</span>';
          $sLabel1 = '<span style="color: grey;">Veuillez maintenant entrer le prix de votre ' . ($iItemArticle + 1) . 'ème article ou 0, 
                                                 si vous n\'en avez pas d\'autres :</span>';
          
          $iItemArticle++;
      } else if ($iPriceArticle === 0) {
          $sLabel1 = '<span style="color: grey;">Votre liste d\'articles est terminée, veuillez effectuer le règlement, svp.</span>';
          $sAnswer = '<span style="color: grey;">Vous devez la somme de ' . $iTotalPrice . ' Euros. <br><br>
                                                 A régler à côté, merci. </span>';
          $sLabel2 = '<span style="color: grey;">Veuillez indiquez la somme que vous donnez pour régler :</span>';
      }
} else {
  $iItemArticle = (int)1;
  $iTotalPrice = (int)0;
}

require "exo_1.html";

?>

<!-- = '<span style="color: grey;"> -->     <!-- </span>' -->