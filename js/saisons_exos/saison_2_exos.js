/* SAISON 2 - Tous les Exercices */


/* EXERCICE 1 */

function double() {
    var iVal = 231
    var iDouble = iVal * 2

    alert(iVal)
    alert(iDouble)
}

/* EXERCICE 2 */

function square() {
    var iNumUser = +prompt("Tapez le nombre dont vous voulez calculer le carré :");
    var iNumSquare = iNumUser * iNumUser;

    alert("Le carré de " + iNumUser + " est " + iNumSquare);
}

/* EXERCICE 3 */

function prixTotalTTC() {
    var iPrixHT = +prompt("Veuille entrer le prix HT de l'article en € :");
    var iTVA = +prompt("Veuille entrer le taux de TVA de l'article en % :");
    var iPrixTotalTTC = iPrixHT + (iPrixHT * iTVA / 100);

    alert("L'article, avec un prix de " + iPrixHT + "€ et une TVA de " + iTVA + "%, a un prix Total de " + iPrixTotalTTC + "€");
}

/* EXERCICE 4 */

function variousSentences() {
    var sMarquise = "belle marquise";
    var sYeux = "vos beaux yeux";
    var sMourir = "me font mourir";
    var sAmour = "d'amour";

    alert("Variante 1 : " + sYeux + ", " + sMarquise + ", " + sMourir + " " + sAmour + ".");
    alert("Variante 2 : " + sYeux + " " + sMourir + " " + sAmour + ", " + sMarquise + ".");
    alert("Variante 3 : " + sAmour + ", " + sMarquise + ", " + sYeux + " " + sMourir + ".");
    alert("Variante 4 : " + sAmour + ", " + sYeux + " " + sMourir + ", " + sMarquise + ".");
}