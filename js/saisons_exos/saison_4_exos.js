/* SAISON 4 - Tous les Exercices */

/* EXERCICE 1 */

function tutuTotoTataJs() {
  var iTutu = Number(document.getElementById("iTutu").value);
  var iToto = Number(document.getElementById("iToto").value);
  var sTata = document.getElementById("sTata").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iTutu <= iToto + 4 && sTata != "OK") {
    iTutu--;
  } else {
    iTutu++;
  }
  sAnswer.innerHTML = "Donc Tutu = " + iTutu;
}

function tutuTotoTataJqr() {
  var iTutu = Number($("#iTutu").val());
  var iToto = Number($("#iToto").val());
  var sTata = $("#sTata").val();
  var sAnswer = $("#answer_formulary");

  if (iTutu <= iToto + 4 && sTata != "OK") {
    iTutu--;
  } else {
    iTutu++;
  }
  sAnswer.html("<b>Donc Tutu = " + iTutu + "</b>");
}

/* EXERCICE 2 */

function predictedTimeJs() {
  var iHour = Number(document.getElementById("iHour").value);
  var iMinute = Number(document.getElementById("iMinute").value);
  var sAnswer = document.getElementById("answer_formulary");

  iMinute++;
  if (iMinute >= 60) {
    iHour++;
    iMinute = iMinute - 60;
    if (iHour >= 24) {
      iHour = iHour - 24;
    }
  }

  if (iHour === 12 && iMinute === 0) {
    sAnswer.innerHTML = "Dans une minute, il sera midi.";
  } else if (iHour === 0 && iMinute === 0) {
    sAnswer.innerHTML = "Dans une minute, il sera minuit.";
  } else {
    if (iMinute < 10) {
      sAnswer.innerHTML =
        "Dans une minute, il sera " + iHour + " H 0" + iMinute + ".";
    } else {
      sAnswer.innerHTML =
        "Dans une minute, il sera " + iHour + " H " + iMinute + ".";
    }
  }
}

function predictedTimeJqr() {
  var iHour = Number($("#iHour").val());
  var iMinute = Number($("#iMinute").val());
  var sAnswer = $("#answer_formulary");

  iMinute++;
  if (iMinute >= 60) {
    iHour++;
    iMinute = iMinute - 60;
    if (iHour >= 24) {
      iHour = iHour - 24;
    }
  }

  if (iHour === 12 && iMinute === 0) {
    sAnswer.html("<b>Dans une minute, il sera midi.</b>");
  } else if (iHour === 0 && iMinute === 0) {
    sAnswer.html("<b>Dans une minute, il sera minuit.</b>");
  } else {
    if (iMinute < 10) {
      sAnswer.html(
        "<b>Dans une minute, il sera " + iHour + " H 0" + iMinute + ".</b>"
      );
    } else {
      sAnswer.html(
        "<b>Dans une minute, il sera " + iHour + " H " + iMinute + ".</b>"
      );
    }
  }
}

/* EXERCICE 3 */

function predictedTimeSecondJs() {
  var iHour = Number(document.getElementById("iHour").value);
  var iMinute = Number(document.getElementById("iMinute").value);
  var iSecond = Number(document.getElementById("iSecond").value);
  var sAnswer = document.getElementById("answer_formulary");

  // Calcul de la nouvelle heure
  iSecond++;
  if (iSecond >= 100) {
    iMinute++;
    iSecond = iSecond - 100;
    if (iMinute >= 60) {
      iHour++;
      iMinute = iMinute - 60;
      if (iHour >= 24) {
        iHour = iHour - 24;
      }
    }
  }

  // Affichage de la nouvelle heure
  if (iHour === 12 && iMinute === 0 && iSecond === 0) {
    sAnswer.innerHTML = "Dans une seconde, il sera midi.";
  } else if (iHour === 0 && iMinute === 0 && iSecond === 0) {
    sAnswer.innerHTML = "Dans une seconde, il sera minuit.";
  } else {
    if (iMinute < 10 && iSecond < 10) {
      sAnswer.innerHTML =
        "Dans une seconde, il sera " +
        iHour +
        " H 0" +
        iMinute +
        " Min et 0" +
        iSecond +
        " Sec.";
    } else if (iMinute < 10 && iSecond >= 10) {
      sAnswer.innerHTML =
        "Dans une seconde, il sera " +
        iHour +
        " H 0" +
        iMinute +
        " Min et " +
        iSecond +
        " Sec.";
    } else if (iMinute >= 10 && iSecond < 10) {
      sAnswer.innerHTML =
        "Dans une seconde, il sera " +
        iHour +
        " H " +
        iMinute +
        " Min et 0" +
        iSecond +
        " Sec.";
    } else {
      sAnswer.innerHTML =
        "Dans une seconde, il sera " +
        iHour +
        " H " +
        iMinute +
        " Min et " +
        iSecond +
        " Sec.";
    }
  }
}

function predictedTimeSecondJqr() {
  var iHour = Number($("#iHour").val());
  var iMinute = Number($("#iMinute").val());
  var iSecond = Number($("#iSecond").val());
  var sAnswer = $("#answer_formulary");

  // Calcul de la nouvelle heure
  iSecond++;
  if (iSecond >= 100) {
    iMinute++;
    iSecond = iSecond - 100;
    if (iMinute >= 60) {
      iHour++;
      iMinute = iMinute - 60;
      if (iHour >= 24) {
        iHour = iHour - 24;
      }
    }
  }

  // Affichage de la nouvelle heure
  if (iHour === 12 && iMinute === 0 && iSecond === 0) {
    sAnswer.html("<b>Dans une seconde, il sera midi.</b>");
  } else if (iHour === 0 && iMinute === 0 && iSecond === 0) {
    sAnswer.html("<b>Dans une seconde, il sera minuit.</b>");
  } else {
    if (iMinute < 10 && iSecond < 10) {
      sAnswer.html(
        "<b>Dans une seconde, il sera " +
          iHour +
          " H 0" +
          iMinute +
          " Min et 0" +
          iSecond +
          " Sec.</b>"
      );
    } else if (iMinute < 10 && iSecond >= 10) {
      sAnswer.html(
        "<b>Dans une seconde, il sera " +
          iHour +
          " H 0" +
          iMinute +
          " Min et " +
          iSecond +
          " Sec.</b>"
      );
    } else if (iMinute >= 10 && iSecond < 10) {
      sAnswer.html(
        "<b>Dans une seconde, il sera " +
          iHour +
          " H " +
          iMinute +
          " Min et 0" +
          iSecond +
          " Sec.</b>"
      );
    } else {
      sAnswer.html(
        "<b>Dans une seconde, il sera " +
          iHour +
          " H " +
          iMinute +
          " Min et " +
          iSecond +
          " Sec.</b>"
      );
    }
  }
}

/* EXERCICE 4 */

function invoicePhotocopyJs() {
  var iNbPhotocopy = Number(document.getElementById("iNbPhotocopy").value);
  var iInvoice;
  var sAnswer = document.getElementById("answer_formulary");

  if (iNbPhotocopy <= 10) {
    iInvoice = iNbPhotocopy * 0.1;
  } else if (iNbPhotocopy > 10 && iNbPhotocopy <= 30) {
    iInvoice = 10 * 0.1 + (iNbPhotocopy - 10) * 0.09;
  } else {
    iInvoice = 10 * 0.1 + 20 * 0.09 + (iNbPhotocopy - 30) * 0.08;
  }
  sAnswer.innerHTML = "Votre facture s'élève à " + iInvoice + "€";
}

function invoicePhotocopyJqr() {
  var iNbPhotocopy = Number($("#iNbPhotocopy").val());
  var iInvoice;
  var sAnswer = $("#answer_formulary");

  if (iNbPhotocopy <= 10) {
    iInvoice = iNbPhotocopy * 0.1;
  } else if (iNbPhotocopy > 10 && iNbPhotocopy <= 30) {
    iInvoice = 10 * 0.1 + (iNbPhotocopy - 10) * 0.09;
  } else {
    iInvoice = 10 * 0.1 + 20 * 0.09 + (iNbPhotocopy - 30) * 0.08;
  }
  sAnswer.html("<b>Votre facture s'élève à " + iInvoice + "€</b>");
}

/* EXERCICE 5 */

function taxableOrNotJs() {
  var iAge = Number(document.getElementById("iAge").value);
  var sSexe = document.getElementById("sSexe").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iAge >= 20 && (sSexe === "homme" || sSexe === "Homme")) {
    sAnswer.innerHTML = "Monsieur, vous êtes imposable.";
  } else if (
    iAge >= 18 &&
    iAge <= 35 &&
    (sSexe === "femme" || sSexe === "Femme")
  ) {
    sAnswer.innerHTML = "Madame, vous êtes imposable.";
  } else {
    sAnswer.innerHTML = "Selon vos renseignements, vous n'êtes pas imposable.";
  }
}

function taxableOrNotJqr() {
  var iAge = Number($("#iAge").val());
  var sSexe = $("#sSexe").val();
  var sAnswer = $("#answer_formulary");

  if (iAge >= 20 && (sSexe === "homme" || sSexe === "Homme")) {
    sAnswer.html("<b>Monsieur, vous êtes imposable.</b>");
  } else if (
    iAge >= 18 &&
    iAge <= 35 &&
    (sSexe === "femme" || sSexe === "Femme")
  ) {
    sAnswer.html("<b>Madame, vous êtes imposable.</b>");
  } else {
    sAnswer.html("<b>Selon vos renseignements, vous n'êtes pas imposable.</b>");
  }
}

/* EXERCICE 6 */

function resultCandidate1Js() {
  var iResultCandidate1 = Number(
    document.getElementById("iResultCandidate1").value
  );
  var iResultCandidate2 = Number(
    document.getElementById("iResultCandidate2").value
  );
  var iResultCandidate3 = Number(
    document.getElementById("iResultCandidate3").value
  );
  var iResultCandidate4 = Number(
    document.getElementById("iResultCandidate4").value
  );
  var sAnswer = document.getElementById("answer_formulary");
  var bSecondRound = iResultCandidate1 >= 12.5 && iResultCandidate1 <= 50;
  var bTheFirst =
    iResultCandidate1 > iResultCandidate2 &&
    iResultCandidate1 > iResultCandidate3 &&
    iResultCandidate1 > iResultCandidate4;
  var bNotOtherAbsoluteMajority =
    iResultCandidate2 <= 50 &&
    iResultCandidate3 <= 50 &&
    iResultCandidate4 <= 50;

  if (iResultCandidate1 > 50) {
    sAnswer.innerHTML = "Le Candidat N°1 est élu dès le 1er tour !";
  } else if (bSecondRound && bTheFirst) {
    sAnswer.innerHTML =
      "Le Candidat N°1 est en ballotage favorable pour le 2nd tour !";
  } else if (bSecondRound && !bTheFirst && bNotOtherAbsoluteMajority) {
    sAnswer.innerHTML =
      "Le Candidat N°1 est en ballotage défavorable pour le 2nd tour !";
  } else {
    sAnswer.innerHTML = "Le Candidat N°1 est battu dès le 1er tour !";
  }
}

function resultCandidate1Jqr() {
  var iResultCandidate1 = Number($("#iResultCandidate1").val());
  var iResultCandidate2 = Number($("#iResultCandidate2").val());
  var iResultCandidate3 = Number($("#iResultCandidate3").val());
  var iResultCandidate4 = Number($("#iResultCandidate4").val());
  var sAnswer = $("#answer_formulary");
  var bSecondRound = iResultCandidate1 >= 12.5 && iResultCandidate1 <= 50;
  var bTheFirst =
    iResultCandidate1 > iResultCandidate2 &&
    iResultCandidate1 > iResultCandidate3 &&
    iResultCandidate1 > iResultCandidate4;
  var bNotOtherAbsoluteMajority =
    iResultCandidate2 <= 50 &&
    iResultCandidate3 <= 50 &&
    iResultCandidate4 <= 50;

  if (iResultCandidate1 > 50) {
    sAnswer.html("<b>Le Candidat N°1 est élu dès le 1er tour !</b>");
  } else if (bSecondRound && bTheFirst) {
    sAnswer.html(
      "<b>Le Candidat N°1 est en ballotage favorable pour le 2nd tour !</b>"
    );
  } else if (bSecondRound && !bTheFirst && bNotOtherAbsoluteMajority) {
    sAnswer.html(
      "<b>Le Candidat N°1 est en ballotage défavorable pour le 2nd tour !</b>"
    );
  } else {
    sAnswer.html("<b>Le Candidat N°1 est battu dès le 1er tour !</b>");
  }
}

/* EXERCICE 7 */

function classCustomersJs() {
  var iAge = Number(document.getElementById("iAge").value);
  var iYearLicence = Number(document.getElementById("iYearLicence").value);
  var iNbCrash = Number(document.getElementById("iNbCrash").value);
  var iSeniorityCustomer = Number(
    document.getElementById("iSeniorityCustomer").value
  );
  var sAnswer = document.getElementById("answer_formulary");
  var bYoungNovice = iAge < 25 && iYearLicence < 2;
  var bMiddleClass =
    (iAge < 25 && iYearLicence >= 2) || (iAge >= 25 && iYearLicence < 2);
  var bOldExpert = iAge >= 25 && iYearLicence >= 2;
  var bNoviceDriver =
    (bYoungNovice && iNbCrash === 0) ||
    (bMiddleClass && iNbCrash === 1) ||
    (bOldExpert && iNbCrash === 2);
  var bMediumDriver =
    (bMiddleClass && iNbCrash === 0) || (bOldExpert && iNbCrash === 1);
  var bExpertDriver = bOldExpert && iNbCrash === 0;
  var bSeniorityCustomer = iSeniorityCustomer >= 5;

  if (bExpertDriver && bSeniorityCustomer) {
    sAnswer.innerHTML = "Vous bénéficiez du Tarif Bleu !";
  } else if (bExpertDriver || (bMediumDriver && bSeniorityCustomer)) {
    sAnswer.innerHTML = "Vous bénéficiez du Tarif Vert !";
  } else if (bMediumDriver || (bNoviceDriver && bSeniorityCustomer)) {
    sAnswer.innerHTML = "Vous bénéficiez du Tarif Orange !";
  } else if (bNoviceDriver) {
    sAnswer.innerHTML = "Vous bénéficiez du Tarif Rouge !";
  } else {
    sAnswer.innerHTML =
      "Au vu de votre situation, nous ne pouvons pas vous assurer.";
  }
}

function classCustomersJqr() {
  var iAge = Number($("#iAge").val());
  var iYearLicence = Number($("#iYearLicence").val());
  var iNbCrash = Number($("#iNbCrash").val());
  var iSeniorityCustomer = Number($("#iSeniorityCustomer").val());
  var sAnswer = $("#answer_formulary");
  var bYoungNovice = iAge < 25 && iYearLicence < 2;
  var bMiddleClass =
    (iAge < 25 && iYearLicence >= 2) || (iAge >= 25 && iYearLicence < 2);
  var bOldExpert = iAge >= 25 && iYearLicence >= 2;
  var bNoviceDriver =
    (bYoungNovice && iNbCrash === 0) ||
    (bMiddleClass && iNbCrash === 1) ||
    (bOldExpert && iNbCrash === 2);
  var bMediumDriver =
    (bMiddleClass && iNbCrash === 0) || (bOldExpert && iNbCrash === 1);
  var bExpertDriver = bOldExpert && iNbCrash === 0;
  var bSeniorityCustomer = iSeniorityCustomer >= 5;

  if (bExpertDriver && bSeniorityCustomer) {
    sAnswer.html("<b>Vous bénéficiez du Tarif Bleu !</b>");
  } else if (bExpertDriver || (bMediumDriver && bSeniorityCustomer)) {
    sAnswer.html("<b>Vous bénéficiez du Tarif Vert !</b>");
  } else if (bMediumDriver || (bNoviceDriver && bSeniorityCustomer)) {
    sAnswer.html("<b>Vous bénéficiez du Tarif Orange !</b>");
  } else if (bNoviceDriver) {
    sAnswer.html("<b>Vous bénéficiez du Tarif Rouge !</b>");
  } else {
    sAnswer.html(
      "<b>Au vu de votre situation, nous ne pouvons pas vous assurer.</b>"
    );
  }
}

/* EXERCICE 8 */

function validDateJs() {
  var iDay = Number(document.getElementById("iDay").value);
  var iMonth = Number(document.getElementById("iMonth").value);
  var iYear = Number(document.getElementById("iYear").value);
  var sAnswer = document.getElementById("answer_formulary");
  var b31 =
    iDay === 31 &&
    (iMonth === 1 ||
      iMonth === 3 ||
      iMonth === 5 ||
      iMonth === 7 ||
      iMonth === 8 ||
      iMonth === 10 ||
      iMonth === 12);
  var bBissextile = iYear % 400 === 0 || (iYear % 4 === 0 && iYear % 100 !== 0);
  var b28 = iDay >= 1 && iDay <= 28 && iMonth === 2;
  var b29 = iDay === 29 && iMonth === 2 && bBissextile;
  var bAllOtherDates =
    iDay >= 1 && iDay <= 30 && iMonth >= 1 && iMonth <= 12 && iMonth !== 2;

  if (b28 || b29 || b31 || bAllOtherDates) {
    sAnswer.innerHTML = "La date est valide !";
  } else {
    sAnswer.innerHTML = "La date est invalide !";
  }
}

function validDateJqr() {
  var iDay = Number($("#iDay").val());
  var iMonth = Number($("#iMonth").val());
  var iYear = Number($("#iYear").val());
  var sAnswer = $("#answer_formulary");
  var b31 =
    iDay === 31 &&
    (iMonth === 1 ||
      iMonth === 3 ||
      iMonth === 5 ||
      iMonth === 7 ||
      iMonth === 8 ||
      iMonth === 10 ||
      iMonth === 12);
  var bBissextile = iYear % 400 === 0 || (iYear % 4 === 0 && iYear % 100 !== 0);
  var b28 = iDay >= 1 && iDay <= 28 && iMonth === 2;
  var b29 = iDay === 29 && iMonth === 2 && bBissextile;
  var bAllOtherDates =
    iDay >= 1 && iDay <= 30 && iMonth >= 1 && iMonth <= 12 && iMonth !== 2;

  if (b28 || b29 || b31 || bAllOtherDates) {
    sAnswer.html("<b>La date est valide !</b>");
  } else {
    sAnswer.html("<b>La date est invalide !</b>");
  }
}
