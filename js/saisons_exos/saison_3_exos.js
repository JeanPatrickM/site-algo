/* SAISON 3 - Tous les Exercices */

/* EXERCICE 1 */

function positiveOrNegativeJs() {
  var iNumber = document.getElementById("first_input_formulary").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumber >= 0) {
    sAnswer.innerHTML = "Votre nombre est positif !";
  } else {
    sAnswer.innerHTML = "Votre nombre est négatif !";
  }
}

function positiveOrNegativeJqr() {
  var iNumber = $("#first_input_formulary").val();
  var sAnswer = $("#answer_formulary");

  if (iNumber >= 0) {
    sAnswer.html("<b>Votre nombre est positif !</b>");
  } else {
    sAnswer.html("<b>Votre nombre est négatif !</b>");
  }
}

/* EXERCICE 2 */

function multiplicationSignJs() {
  var iNumberFirst = document.getElementById("first_input_formulary").value;
  var iNumberSecond = document.getElementById("second_input_formulary").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumberFirst > 0 && iNumberSecond < 0) {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est négative !";
  } else if (iNumberFirst < 0 && iNumberSecond > 0) {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est négative !";
  } else {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est positive !";
  }
}

function multiplicationSignJqr() {
  var iNumberFirst = $("#first_input_formulary").val();
  var iNumberSecond = $("#second_input_formulary").val();
  var sAnswer = $("#answer_formulary");

  if (iNumberFirst > 0 && iNumberSecond < 0) {
    sAnswer.html("<b>La multiplication de ces deux nombres est négative !</b>");
  } else if (iNumberFirst < 0 && iNumberSecond > 0) {
    sAnswer.html("<b>La multiplication de ces deux nombres est négative !</b>");
  } else {
    sAnswer.html("<b>La multiplication de ces deux nombres est positive !</b>");
  }
}

/* EXERCICE 3 */

function alphabeticOrderJs() {
  var sNameFirst = document.getElementById("first_input_formulary").value;
  var sNameSecond = document.getElementById("second_input_formulary").value;
  var sNameThird = document.getElementById("third_input_formulary").value;
  var sNameFirstUpperCase = sNameFirst.toUpperCase();
  var sNameSecondUpperCase = sNameSecond.toUpperCase();
  var sNameThirdUpperCase = sNameThird.toUpperCase();
  var sAnswer = document.getElementById("answer_formulary");

  if (
    sNameFirstUpperCase <= sNameSecondUpperCase &&
    sNameSecondUpperCase <= sNameThirdUpperCase
  ) {
    sAnswer.innerHTML = "Vos noms sont dans l'ordre alphabétique !";
  } else {
    sAnswer.innerHTML = "Vos noms ne sont pas dans l'ordre alphabétique !";
  }
}

function alphabeticOrderJqr() {
  var sNameFirst = $("#first_input_formulary").val();
  var sNameSecond = $("#second_input_formulary").val();
  var sNameThird = $("#third_input_formulary").val();
  var sNameFirstUpperCase = sNameFirst.toUpperCase();
  var sNameSecondUpperCase = sNameSecond.toUpperCase();
  var sNameThirdUpperCase = sNameThird.toUpperCase();
  var sAnswer = $("#answer_formulary");

  if (
    sNameFirstUpperCase <= sNameSecondUpperCase &&
    sNameSecondUpperCase <= sNameThirdUpperCase
  ) {
    sAnswer.html("<b>Vos noms sont dans l'ordre alphabétique !</b>");
  } else {
    sAnswer.html("<b>Vos noms ne sont pas dans l'ordre alphabétique !</b>");
  }
}

/* EXERCICE 4 */

function positiveOrNegativeOrNullJs() {
  var iNumber = document.getElementById("first_input_formulary").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumber > 0) {
    sAnswer.innerHTML = "Votre nombre est positif !";
  } else if (iNumber < 0) {
    sAnswer.innerHTML = "Votre nombre est négatif !";
  } else {
    sAnswer.innerHTML = "Votre nombre est nul !";
  }
}

function positiveOrNegativeOrNullJqr() {
  var iNumber = $("#first_input_formulary").val();
  var sAnswer = $("#answer_formulary");

  if (iNumber > 0) {
    sAnswer.html("<b>Votre nombre est positif !</>");
  } else if (iNumber < 0) {
    sAnswer.html("<b>Votre nombre est négatif !</b>");
  } else {
    sAnswer.html("<b>Votre nombre est nul !</b>");
  }
}

/* EXERCICE 5 */

function multiplicationSignAndNullJs() {
  var iNumberFirst = document.getElementById("first_input_formulary").value;
  var iNumberSecond = document.getElementById("second_input_formulary").value;
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumberFirst == 0 || iNumberSecond == 0) {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est nulle !";
  } else if (iNumberFirst > 0 && iNumberSecond < 0) {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est négative !";
  } else if (iNumberFirst < 0 && iNumberSecond > 0) {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est négative !";
  } else {
    sAnswer.innerHTML = "La multiplication de ces deux nombres est positive !";
  }
}

function multiplicationSignAndNullJqr() {
  var iNumberFirst = $("#first_input_formulary").val();
  var iNumberSecond = $("#second_input_formulary").val();
  var sAnswer = $("#answer_formulary");

  if (iNumberFirst == 0 || iNumberSecond == 0) {
    sAnswer.html("<b>La multiplication de ces deux nombres est nulle !</b>");
  } else if (iNumberFirst > 0 && iNumberSecond < 0) {
    sAnswer.html("<b>La multiplication de ces deux nombres est négative !</b>");
  } else if (iNumberFirst < 0 && iNumberSecond > 0) {
    sAnswer.html("<b>La multiplication de ces deux nombres est négative !</>");
  } else {
    sAnswer.html("<b>La multiplication de ces deux nombres est positive !</b>");
  }
}

/* EXERCICE 6 */

function ageClassJs() {
  var iAgeChildren = document.getElementById("first_input_formulary").value;
  var sAnswer = document.getElementById("answer_formulary");
  var bA = iAgeChildren >= 12;
  var bB = iAgeChildren == 11;
  var bC = iAgeChildren == 10;
  var bD = iAgeChildren == 9;
  var bE = iAgeChildren == 8;
  var bF = iAgeChildren == 7;
  var bG = iAgeChildren == 6;

  if (bA) {
    sAnswer.innerHTML = "Votre enfant est dans la catégorie Cadet.";
  } else if (bC || bB) {
    sAnswer.innerHTML = "Votre enfant est dans la catégorie Minime.";
  } else if (bE || bD) {
    sAnswer.innerHTML = "Votre enfant est dans la catégorie Pupille.";
  } else if (bG || bF) {
    sAnswer.innerHTML = "Votre enfant est dans la catégorie Poussin.";
  } else {
    sAnswer.innerHTML = "Votre enfant est trop jeune pour s'inscrire.";
  }
}

function ageClassJqr() {
  var iAgeChildren = $("#first_input_formulary").val();
  var sAnswer = $("#answer_formulary");
  var bA = iAgeChildren >= 12;
  var bB = iAgeChildren == 11;
  var bC = iAgeChildren == 10;
  var bD = iAgeChildren == 9;
  var bE = iAgeChildren == 8;
  var bF = iAgeChildren == 7;
  var bG = iAgeChildren == 6;

  if (bA) {
    sAnswer.html("<b>Votre enfant est dans la catégorie Cadet.</b>");
  } else if (bC || bB) {
    sAnswer.html("<b>Votre enfant est dans la catégorie Minime.</b>");
  } else if (bE || bD) {
    sAnswer.html("<b>Votre enfant est dans la catégorie Pupille.</b>");
  } else if (bG || bF) {
    sAnswer.html("<b>Votre enfant est dans la catégorie Poussin.</b>");
  } else {
    sAnswer.html("<b>Votre enfant est trop jeune pour s'inscrire.</b>");
  }
}
