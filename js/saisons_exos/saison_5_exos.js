/* SAISON 5 - Tous les Exercices */

/* EXERCICE 1 */

function goodNumberJs() {
  var iNumber = Number(document.getElementById("iNumber").value);
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumber < 1 || iNumber > 3) {
    sAnswer.innerHTML =
      "Saisie erronée. Veuillez ressaisir un nombre qui soit bien compris entre 1 et 3, svp.";
  } else {
    sAnswer.innerHTML = "Bravo, vous avez réussi !";
  }
}

function goodNumberJqr() {
  var iNumber = Number($("#iNumber").val());
  var sAnswer = $("#answer_formulary");

  if (iNumber < 1 || iNumber > 3) {
    sAnswer.html(
      "<b>Saisie erronée. Veuillez ressaisir un nombre qui soit bien compris entre 1 et 3, svp.</b>"
    );
  } else {
    sAnswer.html("<b>Bravo, vous avez réussi !</b>");
  }
}

/* EXERCICE 2_A */

function tenToTwentyJs() {
  var iNumber = Number(document.getElementById("iNumber").value);
  var sAnswer = document.getElementById("answer_formulary");

  if (iNumber < 10 || iNumber > 20) {
    if (iNumber < 10) {
      sAnswer.innerHTML = "Veuillez ressaisir un nombre plus grand, svp.";
    } else {
      sAnswer.innerHTML = "Veuillez ressaisir un nombre plus petit, svp.";
    }
  } else {
    sAnswer.innerHTML = "Bravo, vous avez réussi !";
  }
}

function tenToTwentyJqr() {
  var iNumber = Number($("#iNumber").val());
  var sAnswer = $("#answer_formulary");

  if (iNumber < 10 || iNumber > 20) {
    if (iNumber < 10) {
      sAnswer.html("<b>Veuillez ressaisir un nombre plus grand, svp.</b>");
    } else {
      sAnswer.html("<b>Veuillez ressaisir un nombre plus petit, svp.</b>");
    }
  } else {
    sAnswer.html("<b>Bravo, vous avez réussi !</b>");
  }
}
/* EXERCICE 2_B */

var iNumberChanceJs = Math.floor(Math.random() * 100) + 1;
var iNumberAttemptJs = 0;

function randomOneToHundredJs() {
  var iNumberUser = Number(document.getElementById("iNumberUser").value);
  var sAnswer = document.getElementById("answer_formulary");

  iNumberAttemptJs++;

  if (iNumberUser !== iNumberChanceJs) {
    if (iNumberUser < iNumberChanceJs) {
      sAnswer.innerHTML =
        "Retentez votre chance en saisissant un nombre plus grand.";
    } else {
      sAnswer.innerHTML =
        "Retentez votre chance en saisissant un nombre plus petit.";
    }
  } else {
    sAnswer.innerHTML =
      "Bravo, vous avez réussi en " + iNumberAttemptJs + " coup(s) !";
  }
}

var iNumberChanceJqr = Math.floor(Math.random() * 100) + 1;
var iNumberAttemptJqr = 0;

function randomOneToHundredJqr() {
  var iNumberUser = Number($("#iNumberUser").val());
  var sAnswer = $("#answer_formulary");

  iNumberAttemptJqr++;

  if (iNumberUser !== iNumberChanceJqr) {
    if (iNumberUser < iNumberChanceJqr) {
      sAnswer.html(
        "<b>Retentez votre chance en saisissant un nombre plus grand.</b>"
      );
    } else {
      sAnswer.html(
        "<b>Retentez votre chance en saisissant un nombre plus petit.</b>"
      );
    }
  } else {
    sAnswer.html(
      "<b>Bravo, vous avez réussi en " + iNumberAttemptJqr + " coup(s) !</b>"
    );
  }
}

/* EXERCICE 3 */

function tenNextNumbersJs() {
  var iNumberStart = Number(document.getElementById("iNumberStart").value);
  var sAnswer = document.getElementById("answer_formulary");
  var sResult = "";

  i = 0;

  while (i < 10) {
    iNumberStart++;

    if (i < 9) {
      sResult += iNumberStart + ", ";
    } else {
      sResult += iNumberStart + ". ";
    }

    i++;
  }

  sAnswer.innerHTML = "Les 10 nombres suivants sont : " + sResult;
}

function tenNextNumbersJqr() {
  var iNumberStart = Number($("#iNumberStart").val());
  var sAnswer = $("#answer_formulary");
  var sResult = "";

  i = 0;

  while (i < 10) {
    iNumberStart++;

    if (i < 9) {
      sResult += iNumberStart + ", ";
    } else {
      sResult += iNumberStart + ". ";
    }

    i++;
  }

  sAnswer.html("<b>Les 10 nombres suivants sont : " + sResult + "</b>");
}

/* EXERCICE 4 */

function tenNextNumbersJsFor() {
  var iNumberStart = Number(document.getElementById("iNumberStart").value);
  var sAnswer = document.getElementById("answer_formulary");
  var sResult = "";

  for (i = 0; i < 10; i++) {
    iNumberStart++;

    if (i < 9) {
      sResult += iNumberStart + ", ";
    } else {
      sResult += iNumberStart + ". ";
    }
  }

  sAnswer.innerHTML = "Les 10 nombres suivants sont : " + sResult;
}

function tenNextNumbersJqrFor() {
  var iNumberStart = Number($("#iNumberStart").val());
  var sAnswer = $("#answer_formulary");
  var sResult = "";

  for (i = 0; i < 10; i++) {
    iNumberStart++;

    if (i < 9) {
      sResult += iNumberStart + ", ";
    } else {
      sResult += iNumberStart + ". ";
    }
  }

  sAnswer.html("<b>Les 10 nombres suivants sont : " + sResult + "</b>");
}

/* EXERCICE 5 */

function multiplicationTableJs() {
  var iNumber = Number(document.getElementById("iNumber").value);
  var sAnswer = document.getElementById("answer_formulary");
  var sResult = "";
  var iMultiplication;

  for (i = 1; i <= 10; i++) {
    iMultiplication = iNumber * i;
    sResult +=
      "&nbsp;&nbsp;&nbsp;&nbsp;" +
      iNumber +
      " x " +
      i +
      " = " +
      iMultiplication +
      "<br>";
  }

  sAnswer.innerHTML = "Table de " + iNumber + " :<br><br>" + sResult;
}

function multiplicationTableJqr() {
  var iNumber = Number($("#iNumber").val());
  var sAnswer = $("#answer_formulary");
  var sResult = "";
  var iMultiplication;

  for (i = 1; i <= 10; i++) {
    iMultiplication = iNumber * i;
    sResult +=
      "&nbsp;&nbsp;&nbsp;&nbsp;" +
      iNumber +
      " x " +
      i +
      " = " +
      iMultiplication +
      "<br>";
  }

  sAnswer.html("<b>Table de " + iNumber + " :<br><br>" + sResult + "</b>");
}

/* EXERCICE 6 */

function integerSumJs() {
  var iNumber = parseInt(document.getElementById("iNumber").value);
  var sAnswer = document.getElementById("answer_formulary");
  var iSum = parseInt(0);
  var iSubtraction;

  for (i = iNumber - 1; i >= 0; i--) {
    iSubtraction = iNumber - i;
    iSum += iSubtraction;
  }

  sAnswer.innerHTML =
    "La somme des entiers jusqu'à " + iNumber + " est égale à " + iSum;
}

function integerSumJqr() {
  var iNumber = parseInt($("#iNumber").val());
  var sAnswer = $("#answer_formulary");
  var iSum = parseInt(0);
  var iSubtraction;

  for (i = iNumber - 1; i >= 0; i--) {
    iSubtraction = iNumber - i;
    iSum += iSubtraction;
  }

  sAnswer.html(
    "<b>La somme des entiers jusqu'à " +
      iNumber +
      " est égale à " +
      iSum +
      "</b>"
  );
}

/* EXERCICE 7 */

function factorialJs() {
  var iNumber = parseInt(document.getElementById("iNumber").value);
  var sAnswer = document.getElementById("answer_formulary");
  var iFactorial = 1;

  for (iCount = iNumber - 1; iCount >= 0; iCount--) {
    iFactorial = iFactorial * (iNumber - iCount);
  }

  sAnswer.innerHTML = iNumber + "! est égale à " + iFactorial;
}

function factorialJqr() {
  var iNumber = parseInt($("#iNumber").val());
  var sAnswer = $("#answer_formulary");
  var iFactorial = 1;

  for (iCount = iNumber - 1; iCount >= 0; iCount--) {
    iFactorial = iFactorial * (iNumber - iCount);
  }

  sAnswer.html("<b>" + iNumber + "! est égale à " + iFactorial + "</b>");
}

/* EXERCICE 8 */

var iItemNumberJs = Number(1);
var iLargestNumberJs;
var iPlaceLargestNumberJs;

function theLargestNumberJs() {
  var iNumberUser = Number(document.getElementById("iNumberUser").value);
  var sAnswer = document.getElementById("answer_formulary_with_label");
  var sLabel = document.getElementById("label_input");

  if (iItemNumberJs < 20) {
    if (iItemNumberJs === 1 || iLargestNumberJs < iNumberUser) {
      iLargestNumberJs = iNumberUser;
      iPlaceLargestNumberJs = iItemNumberJs;
    }
    sAnswer.innerHTML =
      "Votre nombre N°" + iItemNumberJs + " est le " + iNumberUser;
    sLabel.innerHTML =
      "Veuillez maintenant entrer votre nombre N°" + (iItemNumberJs + 1) + " :";
  } else {
    if (iLargestNumberJs < iNumberUser) {
      iLargestNumberJs = iNumberUser;
      iPlaceLargestNumberJs = iItemNumberJs;
    }
    sAnswer.innerHTML =
      "Votre nombre N°" +
      iItemNumberJs +
      " est le " +
      iNumberUser +
      "<br><br>Le plus grand nombre que vous avez entré est le " +
      iLargestNumberJs +
      "<br><br>Et c'était le nombre N°" +
      iPlaceLargestNumberJs +
      " !";
    sLabel.innerHTML =
      "Vous avez entré vos " +
      iItemNumberJs +
      " nombres, regardez quel est le plus grand, à côté.";
  }

  iItemNumberJs++;
}

var iItemNumberJqr = Number(1);
var iLargestNumberJqr;
var iPlaceLargestNumberJqr;

function theLargestNumberJqr() {
  var iNumberUser = Number($("#iNumberUser").val());
  var sAnswer = $("#answer_formulary_with_label");
  var sLabel = $("#label_input");

  if (iItemNumberJqr < 20) {
    if (iItemNumberJqr === 1 || iLargestNumberJqr < iNumberUser) {
      iLargestNumberJqr = iNumberUser;
      iPlaceLargestNumberJqr = iItemNumberJqr;
    }
    sAnswer.html(
      "<b>Votre nombre N°" + iItemNumberJqr + " est le " + iNumberUser + "</b>"
    );
    sLabel.html(
      "<b>Veuillez maintenant entrer votre nombre N°" +
        (iItemNumberJqr + 1) +
        " :</b>"
    );
  } else {
    if (iLargestNumberJqr < iNumberUser) {
      iLargestNumberJqr = iNumberUser;
      iPlaceLargestNumberJqr = iItemNumberJqr;
    }
    sAnswer.html(
      "<b>Votre nombre N°" +
        iItemNumberJqr +
        " est le " +
        iNumberUser +
        "<br><br>Le plus grand nombre que vous avez entré est le " +
        iLargestNumberJqr +
        "<br><br>Et c'était le nombre N°" +
        iPlaceLargestNumberJqr +
        " !</b>"
    );
    sLabel.html(
      "<b>Vous avez entré vos " +
        iItemNumberJqr +
        " nombres, regardez quel est le plus grand, à côté.</b>"
    );
  }

  iItemNumberJqr++;
}

/* EXERCICE 9 */

var iItemNumberJs = Number(1);
var iLargestNumberJs;
var iPlaceLargestNumberJs;

function theLargestNumberJs() {
  var iNumberUser = Number(document.getElementById("iNumberUser").value);
  var sAnswer = document.getElementById("answer_formulary_with_label");
  var sLabel = document.getElementById("label_input");

  if (iNumberUser !== 0) {
    if (iItemNumberJs === 1 || iLargestNumberJs < iNumberUser) {
      iLargestNumberJs = iNumberUser;
      iPlaceLargestNumberJs = iItemNumberJs;
    }
    sAnswer.innerHTML =
      "Votre nombre N°" + iItemNumberJs + " est le " + iNumberUser;
    sLabel.innerHTML =
      "Veuillez maintenant entrer votre nombre N°" + (iItemNumberJs + 1) + " :";

    iItemNumberJs++;
  } else {
    if (iLargestNumberJs < 0) {
      iLargestNumberJs = 0;
      iPlaceLargestNumberJs = iItemNumberJs;
    }
    sAnswer.innerHTML =
      "Votre nombre N°" +
      iItemNumberJs +
      " est le 0 <br><br> Le plus grand nombre que vous avez entré est le " +
      iLargestNumberJs +
      "<br><br>Et c'était le nombre N°" +
      iPlaceLargestNumberJs +
      " !";
    sLabel.innerHTML =
      "Vous avez entré un 0, c'est donc terminé, regardez à côté quel est le plus grand nombre entré.";
  }
}

var iItemNumberJqr = Number(1);
var iLargestNumberJqr;
var iPlaceLargestNumberJqr;

function theLargestNumberJqr() {
  var iNumberUser = Number($("#iNumberUser").val());
  var sAnswer = $("#answer_formulary_with_label");
  var sLabel = $("#label_input");

  if (iNumberUser !== 0) {
    if (iItemNumberJqr === 1 || iLargestNumberJqr < iNumberUser) {
      iLargestNumberJqr = iNumberUser;
      iPlaceLargestNumberJqr = iItemNumberJqr;
    }
    sAnswer.html(
      "<b>Votre nombre N°" + iItemNumberJqr + " est le " + iNumberUser + "</b>"
    );
    sLabel.html(
      "<b>Veuillez maintenant entrer votre nombre N°" +
        (iItemNumberJqr + 1) +
        " :</b>"
    );

    iItemNumberJqr++;
  } else {
    if (iLargestNumberJqr < 0) {
      iLargestNumberJqr = 0;
      iPlaceLargestNumberJqr = iItemNumberJqr;
    }
    sAnswer.html(
      "<b>Votre nombre N°" +
        iItemNumberJqr +
        " est le 0 <br><br> Le plus grand nombre que vous avez entré est le " +
        iLargestNumberJqr +
        "<br><br>Et c'était le nombre N°" +
        iPlaceLargestNumberJqr +
        " !</b>"
    );
    sLabel.html(
      "<b>Vous avez entré un 0, c'est donc terminé, regardez à côté quel est le plus grand nombre entré.</b>"
    );
  }
}

/* EXERCICE 10 */

var iItemArticleJs = parseInt(1);
var iTotalPriceJs = parseInt(0);

function giveChangeJs() {
  var iPriceArticle = parseInt(document.getElementById("iPriceArticle").value);
  var iCash = parseInt(document.getElementById("iCash").value);
  var sAnswer = document.getElementById("answer_formulary_with_two_label");
  var sLabel1 = document.getElementById("label_input_1");
  var sLabel2 = document.getElementById("label_input_2");
  var iChange;
  var sChange = "";

  if (iCash < iTotalPriceJs && iPriceArticle === 0) {
    sAnswer.innerHTML =
      "Désolé, vous n'avez pas donné assez, veuillez indiquer une somme d'argent supérieure ou égale à " +
      iTotalPriceJs +
      " Euros, au même endroit.";
  } else if (iCash === iTotalPriceJs && iPriceArticle === 0) {
    sLabel2.innerHTML = "Votre règlement a bien été effectué.";
    sAnswer.innerHTML = "Merci, nous vous souhaitons une bonne journée !";
  } else if (iCash > iTotalPriceJs && iPriceArticle === 0) {
    sLabel2.innerHTML =
      "Votre règlement a bien été effectué, nous allons vous rendre votre monnaie, à côté.";
    iChange = iCash - iTotalPriceJs;

    while (iChange >= 10) {
      iChange -= 10;
      sChange += "10 Euros ";
    }

    if (iChange >= 5) {
      iChange -= 5;
      sChange += "5 Euros ";
    }

    while (iChange !== 0) {
      iChange--;
      sChange += "1 Euros ";
    }

    sAnswer.innerHTML =
      "Merci, voilà votre monnaie concernant votre facture de " +
      iTotalPriceJs +
      " Euros : <br><br>" +
      sChange +
      "<br><br>Nous vous souhaitons une bonne journée !";
  } else if (iPriceArticle !== 0) {
    iTotalPriceJs += iPriceArticle;

    sAnswer.innerHTML =
      "Votre article N°" +
      iItemArticleJs +
      " coûte " +
      iPriceArticle +
      " Euros.";
    sLabel1.innerHTML =
      "Veuillez maintenant entrer le prix de votre " +
      (iItemArticleJs + 1) +
      "ème article ou 0, si vous n'en avez pas d'autres :";

    iItemArticleJs++;
  } else if (iPriceArticle === 0) {
    sLabel1.innerHTML =
      "Votre liste d'articles est terminée, veuillez effectuer le règlement, svp.";
    sAnswer.innerHTML =
      "Vous devez la somme de " +
      iTotalPriceJs +
      " Euros. <br><br>A régler à côté, merci. ";
    sLabel2.innerHTML =
      "Veuillez indiquez la somme que vous donnez pour régler :";
  }
}

var iItemArticleJqr = parseInt(1);
var iTotalPriceJqr = parseInt(0);

function giveChangeJqr() {
  var iPriceArticle = parseInt($("#iPriceArticle").val());
  var iCash = parseInt($("#iCash").val());
  var sAnswer = $("#answer_formulary_with_two_label");
  var sLabel1 = $("#label_input_1");
  var sLabel2 = $("#label_input_2");
  var iChange;
  var sChange = "";

  if (iCash < iTotalPriceJqr && iPriceArticle === 0) {
    sAnswer.html(
      "<b>Désolé, vous n'avez pas donné assez, veuillez indiquer une somme d'argent supérieure ou égale à " +
        iTotalPriceJqr +
        " Euros, au même endroit.</b>"
    );
  } else if (iCash === iTotalPriceJqr && iPriceArticle === 0) {
    sLabel2.html("<b>Votre règlement a bien été effectué.</b>");
    sAnswer.html("<b>Merci, nous vous souhaitons une bonne journée !</b>");
  } else if (iCash > iTotalPriceJqr && iPriceArticle === 0) {
    sLabel2.html(
      "<b>Votre règlement a bien été effectué, nous allons vous rendre votre monnaie, à côté.</b>"
    );
    iChange = iCash - iTotalPriceJqr;

    while (iChange >= 10) {
      iChange -= 10;
      sChange += "10 Euros ";
    }

    if (iChange >= 5) {
      iChange -= 5;
      sChange += "5 Euros ";
    }

    while (iChange !== 0) {
      iChange--;
      sChange += "1 Euros ";
    }

    sAnswer.html(
      "<b>Merci, voilà votre monnaie concernant votre facture de " +
        iTotalPriceJqr +
        " Euros : <br><br>" +
        sChange +
        "<br><br>Nous vous souhaitons une bonne journée !</b>"
    );
  } else if (iPriceArticle !== 0) {
    iTotalPriceJqr += iPriceArticle;

    sAnswer.html(
      "<b>Votre article N°" +
        iItemArticleJqr +
        " coûte " +
        iPriceArticle +
        " Euros.</b>"
    );
    sLabel1.html(
      "<b>Veuillez maintenant entrer le prix de votre " +
        (iItemArticleJqr + 1) +
        "ème article ou 0, si vous n'en avez pas d'autres :</b>"
    );

    iItemArticleJqr++;
  } else if (iPriceArticle === 0) {
    sLabel1.html(
      "<b>Votre liste d'articles est terminée, veuillez effectuer le règlement, svp.</b>"
    );
    sAnswer.html(
      "<b>Vous devez la somme de " +
        iTotalPriceJqr +
        " Euros. <br><br>A régler à côté, merci. </b>"
    );
    sLabel2.html(
      "<b>Veuillez indiquez la somme que vous donnez pour régler :</b>"
    );
  }
}

/*Exercice 11 */

function horseRacingJs() {
  var iHorses = parseInt(document.getElementById("iHorses").value);
  var iPlayedHorses = parseInt(document.getElementById("iPlayedHorses").value);
  var sAnswer = document.getElementById("answer_formulary");
  iX = 1;
  iFactorialPlayedHorses = 1;

  if (iHorses >= iPlayedHorses) {
    for (iCount = iPlayedHorses - 1; iCount >= 0; iCount--) {
      iX = iX * (iHorses - iCount);
      iFactorialPlayedHorses =
        iFactorialPlayedHorses * (iPlayedHorses - iCount);
    }

    iY = iX / iFactorialPlayedHorses;
    sAnswer.innerHTML =
      "Dans l'ordre, vous avez 1 chance sur " +
      iX +
      " de gagner. <br><br>Dans le désordre, vous avez 1 chance sur " +
      iY +
      " de gagner.";
  } else {
    sAnswer.innerHTML =
      "Votre saisie est incorrect, vous ne pouvez pas jouer plus de chevaux que le nombre qui participe. <br><br>Veuillez indiquer des valeurs correctes, svp.";
  }
}

function horseRacingJqr() {
  var iHorses = parseInt($("#iHorses").val());
  var iPlayedHorses = parseInt($("#iPlayedHorses").val());
  var sAnswer = $("#answer_formulary");
  iX = 1;
  iFactorialPlayedHorses = 1;

  if (iHorses >= iPlayedHorses) {
    for (iCount = iPlayedHorses - 1; iCount >= 0; iCount--) {
      iX = iX * (iHorses - iCount);
      iFactorialPlayedHorses =
        iFactorialPlayedHorses * (iPlayedHorses - iCount);
    }

    iY = iX / iFactorialPlayedHorses;
    sAnswer.html(
      "<b>Dans l'ordre, vous avez 1 chance sur " +
        iX +
        " de gagner. <br><br>Dans le désordre, vous avez 1 chance sur " +
        iY +
        " de gagner.</b>"
    );
  } else {
    sAnswer.html(
      "<b>Votre saisie est incorrect, vous ne pouvez pas jouer plus de chevaux que le nombre qui participe. <br><br>Veuillez indiquer des valeurs correctes, svp.</b>"
    );
  }
}
