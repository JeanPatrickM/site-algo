/* SAISON 6 - Tous les Exercices */

/* EXERCICE 1 */

function nullArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aArray = [];
  var sArray = "";

  for (iCount = 0; iCount <= 6; iCount++) {
    aArray[iCount] = 0;
    sArray = sArray + "Index " + iCount + " : " + aArray[iCount] + "<br>";
  }

  sAnswer.innerHTML = sArray;
}

function nullArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aArray = [];
  var sArray = "";

  for (iCount = 0; iCount <= 6; iCount++) {
    aArray[iCount] = 0;
    sArray += "Index " + iCount + " : " + aArray[iCount] + "<br>";
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 2 */

function vowelsArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aArray = [];
  var sArray = "";
  var aVowels = ["a", "e", "i", "o", "u", "y"];

  for (iCount = 0; iCount <= 5; iCount++) {
    aArray[iCount] = aVowels[iCount];
    sArray = sArray + "Index " + iCount + " : " + aArray[iCount] + "<br>";
  }

  sAnswer.innerHTML = sArray;
}

function vowelsArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aArray = [];
  var sArray = "";
  var aVowels = ["a", "e", "i", "o", "u", "y"];

  for (iCount = 0; iCount <= 5; iCount++) {
    aArray[iCount] = aVowels[iCount];
    sArray += "Index " + iCount + " : " + aArray[iCount] + "<br>";
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 3 */

var iItemScoreJs = 1;
var sArray = "";

function scoresArrayJs() {
  var iScore = Number(document.getElementById("iScore").value);
  var sAnswer = document.getElementById("answer_formulary_with_label");
  var sLabel = document.getElementById("label_input");
  var aScores = [];

  if (iItemScoreJs < 9) {
    aScores[iItemScoreJs - 1] = iScore;
    sArray =
      sArray +
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br>";
    sAnswer.innerHTML =
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br><br>Poursuivez à côté, merci.";

    iItemScoreJs++;
    sLabel.innerHTML = "Indiquez maintenant la note N°" + iItemScoreJs + " :";
  } else {
    aScores[iItemScoreJs - 1] = iScore;
    sArray =
      sArray +
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br>";

    sLabel.innerHTML =
      "Vous avez entré toutes les notes, regardez le récapitulatif à côté.";
    sAnswer.innerHTML =
      "Voici les notes que vous avez indiquées :<br><br>" + sArray;
  }
}

var iItemScoreJqr = 1;
var sArrayJqr = "";

function scoresArrayJqr() {
  var iScore = Number($("#iScore").val());
  var sAnswer = $("#answer_formulary_with_label");
  var sLabel = $("#label_input");
  var aScores = [];

  if (iItemScoreJqr < 9) {
    aScores[iItemScoreJqr - 1] = iScore;
    sArrayJqr =
      sArrayJqr +
      "Note N°" +
      iItemScoreJqr +
      " : " +
      aScores[iItemScoreJqr - 1] +
      "<br>";
    sAnswer.html(
      "<b>Note N°" +
        iItemScoreJqr +
        " : " +
        aScores[iItemScoreJqr - 1] +
        "<br><br>Poursuivez à côté, merci.</b>"
    );

    iItemScoreJqr++;
    sLabel.html("<b>Indiquez maintenant la note N°" + iItemScoreJqr + " :</b>");
  } else {
    aScores[iItemScoreJqr - 1] = iScore;
    sArrayJqr =
      sArrayJqr +
      "Note N°" +
      iItemScoreJqr +
      " : " +
      aScores[iItemScoreJqr - 1] +
      "<br>";

    sLabel.html(
      "<b>Vous avez entré toutes les notes, regardez le récapitulatif à côté.</b>"
    );
    sAnswer.html(
      "<b>Voici les notes que vous avez indiquées :<br><br>" +
        sArrayJqr +
        "</b>"
    );
  }
}

/* EXERCICE 4 */

function squareArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aNbr = [];
  var sArray = "";

  for (iCount = 0; iCount <= 5; iCount++) {
    aNbr[iCount] = iCount * iCount;
    sArray = sArray + "Index " + iCount + " : " + aNbr[iCount] + "<br>";
  }

  sAnswer.innerHTML = sArray;
}

function squareArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aNbr = [];
  var sArray = "";

  for (iCount = 0; iCount <= 5; iCount++) {
    aNbr[iCount] = iCount * iCount;
    sArray += "Index " + iCount + " : " + aNbr[iCount] + "<br>";
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 5 */

function addTwoArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aN = [];
  var sArray = "";

  for (iCount = 0; iCount <= 6; iCount++) {
    if (iCount === 0) {
      aN[iCount] = 1;
      sArray = "Index " + iCount + " : " + aN[iCount] + "<br>";
    } else {
      aN[iCount] = aN[iCount - 1] + 2;
      sArray += "Index " + iCount + " : " + aN[iCount] + "<br>";
    }
  }

  sAnswer.innerHTML = sArray;
}

function addTwoArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aN = [];
  var sArray = "";

  for (iCount = 0; iCount <= 6; iCount++) {
    if (iCount === 0) {
      aN[iCount] = 1;
      sArray = "Index " + iCount + " : " + aN[iCount] + "<br>";
    } else {
      aN[iCount] = aN[iCount - 1] + 2;
      sArray += "Index " + iCount + " : " + aN[iCount] + "<br>";
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 6 */

function sequenceArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aSequence = [];
  var sArray = "";

  for (iCount = 0; iCount <= 7; iCount++) {
    if (iCount === 0 || iCount === 1) {
      aSequence[iCount] = 1;
      sArray += "Index " + iCount + " : " + aSequence[iCount] + "<br>";
    } else {
      aSequence[iCount] = aSequence[iCount - 1] + aSequence[iCount - 2];
      sArray += "Index " + iCount + " : " + aSequence[iCount] + "<br>";
    }
  }

  sAnswer.innerHTML = sArray;
}

function sequenceArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aSequence = [];
  var sArray = "";

  for (iCount = 0; iCount <= 7; iCount++) {
    if (iCount === 0 || iCount === 1) {
      aSequence[iCount] = 1;
      sArray += "Index " + iCount + " : " + aSequence[iCount] + "<br>";
    } else {
      aSequence[iCount] = aSequence[iCount - 1] + aSequence[iCount - 2];
      sArray += "Index " + iCount + " : " + aSequence[iCount] + "<br>";
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 7 */

var iItemScoreJs = 1;
var iAverageJs = 0;
var sArray = "";

function averageArrayJs() {
  var iScore = Number(document.getElementById("iScore").value);
  var sAnswer = document.getElementById("answer_formulary_with_label");
  var sLabel = document.getElementById("label_input");
  var aScores = [];

  if (iItemScoreJs < 9) {
    aScores[iItemScoreJs - 1] = iScore;
    sArray =
      sArray +
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br>";
    iAverageJs += iScore / 9;
    sAnswer.innerHTML =
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br><br>Poursuivez à côté, merci.";

    iItemScoreJs++;
    document.getElementById("iScore").value = "";
    sLabel.innerHTML = "Indiquez maintenant la note N°" + iItemScoreJs + " :";
  } else {
    aScores[iItemScoreJs - 1] = iScore;
    sArray =
      sArray +
      "Note N°" +
      iItemScoreJs +
      " : " +
      aScores[iItemScoreJs - 1] +
      "<br>";
    iAverageJs += iScore / 9;

    sLabel.innerHTML =
      "Vous avez entré toutes les notes, regardez le récapitulatif à côté.";
    sAnswer.innerHTML =
      "Voici les notes que vous avez indiquées :<br><br>" +
      sArray +
      "<br><br>Et la moyenne de ces notes est : " +
      iAverageJs;
  }
}

var iItemScoreJqr = 1;
var iAverageJqr = 0;
var sArrayJqr = "";

function averageArrayJqr() {
  var iScore = Number($("#iScore").val());
  var sAnswer = $("#answer_formulary_with_label");
  var sLabel = $("#label_input");
  var aScores = [];

  if (iItemScoreJqr < 9) {
    aScores[iItemScoreJqr - 1] = iScore;
    sArrayJqr =
      sArrayJqr +
      "Note N°" +
      iItemScoreJqr +
      " : " +
      aScores[iItemScoreJqr - 1] +
      "<br>";
    iAverageJqr += iScore / 9;
    sAnswer.html(
      "<b>Note N°" +
        iItemScoreJqr +
        " : " +
        aScores[iItemScoreJqr - 1] +
        "<br><br>Poursuivez à côté, merci.</b>"
    );

    iItemScoreJqr++;
    $("#iScore").val() = "";
    sLabel.html("<b>Indiquez maintenant la note N°" + iItemScoreJqr + " :</b>");
  } else {
    aScores[iItemScoreJqr - 1] = iScore;
    sArrayJqr =
      sArrayJqr +
      "Note N°" +
      iItemScoreJqr +
      " : " +
      aScores[iItemScoreJqr - 1] +
      "<br>";
    iAverageJqr += iScore / 9;

    sLabel.html(
      "<b>Vous avez entré toutes les notes, regardez le récapitulatif à côté.</b>"
    );
    sAnswer.html(
      "<b>Voici les notes que vous avez indiquées :<br><br>" +
        sArrayJqr +
        "<br><br>Et la moyenne de ces notes est : " +
        iAverageJqr +
        "</b>"
    );
  }
}

/* EXERCICE 8 */

function positiveOrNegativeArrayJs() {
  var iNbrValue = Number(document.getElementById("iNbrValue").value);
  var iNbr;
  var sAnswer = document.getElementById("answer_formulary_medium");
  var aPositiveOrNegative = [];
  var iNegativeNbr = 0;
  var iPositiveNbr = 0;
  var sArray = "";

  for (iCount = 1; iCount <= iNbrValue; iCount++) {
    iNbr = Number(document.getElementById("iNbr_" + iCount).value);

    aPositiveOrNegative[iCount - 1] = iNbr;

    if (iNbr < 0) {
      iNegativeNbr++;
    } else {
      iPositiveNbr++;
    }

    if (iNbrValue === 1 || iNbrValue === iCount) {
      sArray += aPositiveOrNegative[iCount - 1];
    } else {
      sArray += aPositiveOrNegative[iCount - 1] + ", ";
    }
  }

  sAnswer.innerHTML =
    "Le tableau créé est donc :<br><br>aPositiveOrNegative = [" +
    sArray +
    "] <br><br>Et il s'y trouve " +
    iNegativeNbr +
    " valeur(s) négative(s), et " +
    iPositiveNbr +
    " valeur(s) positive(s). <br><br>Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.";

  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display =
    "none";
  document.getElementById("btn_reset_nbr_value").style.display = "none";
  document.getElementById("input_nbr_value").innerHTML = "";
  document.getElementById("iNbrValue").value = "";
  document.getElementById("btn_nbr_value").style.display = "inline";
}

function positiveOrNegativeArrayJqr() {
  var iNbrValue = Number($("#iNbrValue").val());
  var iNbr;
  var sAnswer = $("#answer_formulary_medium");
  var aPositiveOrNegative = [];
  var iNegativeNbr = 0;
  var iPositiveNbr = 0;
  var sArray = "";

  for (iCount = 1; iCount <= iNbrValue; iCount++) {
    iNbr = Number($("#iNbr_" + iCount).val());

    aPositiveOrNegative[iCount - 1] = iNbr;

    if (iNbr < 0) {
      iNegativeNbr++;
    } else {
      iPositiveNbr++;
    }

    if (iNbrValue === 1 || iNbrValue === iCount) {
      sArray += aPositiveOrNegative[iCount - 1];
    } else {
      sArray += aPositiveOrNegative[iCount - 1] + ", ";
    }
  }

  sAnswer.html(
    "<b>Le tableau créé est donc :<br><br>aPositiveOrNegative = [" +
      sArray +
      "] <br><br>Et il s'y trouve " +
      iNegativeNbr +
      " valeur(s) négative(s), et " +
      iPositiveNbr +
      " valeur(s) positive(s). <br><br>Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.</b>"
  );

  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();
}

/* EXERCICE 9 */

/* EXERCICE 10 */

/* EXERCICE 11 */

function toonCalculationJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aArray_1 = [4, 8, 7, 12];
  var aArray_2 = [3, 6];
  var iNbrValueArray_1 = 4;
  var iNbrValueArray_2 = 2;
  var iResult = 0;
  var sArray = "";

  for (iCount = 1; iCount <= iNbrValueArray_2; iCount++) {
    for (jCount = 1; jCount <= iNbrValueArray_1; jCount++) {
      iResult += aArray_2[iCount - 1] * aArray_1[jCount - 1];

      if (jCount < iNbrValueArray_1 || iCount < iNbrValueArray_2) {
        sArray += aArray_2[iCount - 1] + " * " + aArray_1[jCount - 1] + " + ";
      } else {
        sArray += aArray_2[iCount - 1] + " * " + aArray_1[jCount - 1];
      }
    }
  }

  sAnswer.innerHTML =
    "Le 'Toon' de ces 2 tableaux est donc : <br><br>" +
    sArray +
    " = " +
    iResult;
}

function toonCalculationJqr() {
  var sAnswer = $("#answer_formulary");
  var aArray_1 = [4, 8, 7, 12];
  var aArray_2 = [3, 6];
  var iNbrValueArray_1 = 4;
  var iNbrValueArray_2 = 2;
  var iResult = 0;
  var sArray = "";

  for (iCount = 1; iCount <= iNbrValueArray_2; iCount++) {
    for (jCount = 1; jCount <= iNbrValueArray_1; jCount++) {
      iResult += aArray_2[iCount - 1] * aArray_1[jCount - 1];

      if (jCount < iNbrValueArray_1 || iCount < iNbrValueArray_2) {
        sArray += aArray_2[iCount - 1] + " * " + aArray_1[jCount - 1] + " + ";
      } else {
        sArray += aArray_2[iCount - 1] + " * " + aArray_1[jCount - 1];
      }
    }
  }

  sAnswer.html(
    "<b>Le 'Toon' de ces 2 tableaux est donc : <br><br>" +
      sArray +
      " = " +
      iResult +
      "</b>"
  );
}

/* EXERCICE 12 */

/* EXERCICE 13 */

function maxValueArrayJs() {
  var iNbrValue = Number(document.getElementById("iNbrValue").value);
  var sAnswer = document.getElementById("answer_formulary_medium");
  var aValue = [];
  var sArray = "";
  var iNbr, iMaxValue, iIndexMax;

  for (iCount = 0; iCount <= iNbrValue - 1; iCount++) {
    iNbr = Number(document.getElementById("iNbr_" + (iCount + 1)).value);

    aValue[iCount] = iNbr;

    if (iCount === 0 || aValue[iCount] > iMaxValue) {
      iMaxValue = aValue[iCount];
      iIndexMax = iCount;
    }

    if (iNbrValue === 1 || iNbrValue === iCount + 1) {
      sArray += aValue[iCount];
    } else {
      sArray += aValue[iCount] + ", ";
    }
  }

  sAnswer.innerHTML =
    "Le tableau créé est donc :<br><br> aValue = [" +
    sArray +
    "]<br><br> Sa plus grande valeur est " +
    iMaxValue +
    ", elle se trouve à l'index N°" +
    iIndexMax +
    ", et cette valeur est donc en position " +
    (iIndexMax + 1) +
    " dans ce tableau.<br><br> Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.";

  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display =
    "none";
  document.getElementById("btn_reset_nbr_value").style.display = "none";
  document.getElementById("input_nbr_value").innerHTML = "";
  document.getElementById("iNbrValue").value = "";
  document.getElementById("btn_nbr_value").style.display = "inline";
}

function maxValueArrayJqr() {
  var iNbrValue = Number($("#iNbrValue").val());
  var sAnswer = $("#answer_formulary_medium");
  var aValue = [];
  var sArray = "";
  var iNbr, iMaxValue, iIndexMax;

  for (iCount = 0; iCount <= iNbrValue - 1; iCount++) {
    iNbr = Number($("#iNbr_" + (iCount + 1)).val());

    aValue[iCount] = iNbr;

    if (iCount === 0 || aValue[iCount] > iMaxValue) {
      iMaxValue = aValue[iCount];
      iIndexMax = iCount;
    }

    if (iNbrValue === 1 || iNbrValue === iCount + 1) {
      sArray += aValue[iCount];
    } else {
      sArray += aValue[iCount] + ", ";
    }
  }

  sAnswer.html(
    "<b>Le tableau créé est donc :<br><br> aValue = [" +
      sArray +
      "]<br><br> Sa plus grande valeur est " +
      iMaxValue +
      ", elle se trouve à l'index N°" +
      iIndexMax +
      ", et cette valeur est donc en position " +
      (iIndexMax + 1) +
      " dans ce tableau.<br><br> Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.</b>"
  );

  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();
}

/* EXERCICE 14 */

/* EXERCICE 15 */

/* EXERCICE 16 */

/* EXERCICE 17 */

function hundredValueArrayJs() {
  var sAnswer = document.getElementById("answer_formulary_medium");
  var aHundredValue = [];
  var sArray = "";
  var iNbr, iTemporal, iCount, jCount, kCount, mCount;

  // Attribution et triage des valeurs
  for (iCount = 0; iCount < 100; iCount++) {
    iNbr = Number(document.getElementById("iNbr_" + (iCount + 1)).value);

    aHundredValue[iCount] = iNbr;

    for (jCount = 0; jCount <= iCount - 1; jCount++) {
      if (aHundredValue[iCount] < aHundredValue[jCount]) {
        iTemporal = aHundredValue[iCount];

        for (kCount = iCount; kCount >= jCount + 1; kCount--) {
          aHundredValue[kCount] = aHundredValue[kCount - 1];
        }

        aHundredValue[jCount] = iTemporal;
        break;
      }
    }
  }

  // Affichage des valeurs
  for (mCount = 0; mCount < 100; mCount++) {
    if (mCount < 99) {
      sArray += aHundredValue[mCount] + ", ";
    } else {
      sArray += aHundredValue[mCount];
    }
  }

  sAnswer.innerHTML =
    "Voici le tableau avec les 100 valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aHundredValue[" +
    sArray +
    "]";

  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_value_specified").style.display = "inline";
  document.getElementById("input_nbr_value").innerHTML = "";
}

function hundredValueArrayJqr() {
  var sAnswer = $("#answer_formulary_medium");
  var aHundredValue = [];
  var sArray = "";
  var iNbr, iTemporal, iCount, jCount, kCount, mCount;

  // Attribution et triage des valeurs
  for (iCount = 0; iCount < 100; iCount++) {
    iNbr = Number($("#iNbr_" + (iCount + 1)).val());

    aHundredValue[iCount] = iNbr;

    for (jCount = 0; jCount <= iCount - 1; jCount++) {
      if (aHundredValue[iCount] < aHundredValue[jCount]) {
        iTemporal = aHundredValue[iCount];

        for (kCount = iCount; kCount >= jCount + 1; kCount--) {
          aHundredValue[kCount] = aHundredValue[kCount - 1];
        }

        aHundredValue[jCount] = iTemporal;
        break;
      }
    }
  }

  // Affichage des valeurs
  for (mCount = 0; mCount < 100; mCount++) {
    if (mCount < 99) {
      sArray += aHundredValue[mCount] + ", ";
    } else {
      sArray += aHundredValue[mCount];
    }
  }

  sAnswer.html(
    "<b>Voici le tableau avec les 100 valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aHundredValue[" +
      sArray +
      "]</b>"
  );

  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_nbr_value_specified").show();
  $("#input_nbr_value").html("");
}

/* Ces éléments sont à ajouter à la fin des fonctions, pour les exercices où l'on a besoin de faire choisir à l'utilisateur le nombre de valeur à indiquer

// A ajouter à la fin de la fonction de l'exercice, en Javascript
  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display = "none";
  document.getElementById("btn_reset_nbr_value").style.display = "none";
  document.getElementById("input_nbr_value").innerHTML = "";
  document.getElementById("iNbrValue").value = "";
  document.getElementById("btn_nbr_value").style.display = "inline";

// A ajouter à la fin de la fonction de l'exercice, en JQuery
  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();

*/
