/* SAISON 7 - Tous les Exercices */

/* EXERCICE 1 */

function nbrConsecutiveOrNotJs() {
  var iNbrValue = Number(document.getElementById("iNbrValue").value);
  var sAnswer = document.getElementById("answer_formulary_medium");
  var aValue = [];
  var sArray = "";
  var bConsecutive = false;
  var iNbr, iTemporal, iCount, jCount, kCount, mCount, nCount;

  // Attribution et triage des valeurs
  for (iCount = 0; iCount < iNbrValue; iCount++) {
    iNbr = Number(document.getElementById("iNbr_" + (iCount + 1)).value);

    aValue[iCount] = iNbr;

    for (jCount = 0; jCount <= iCount - 1; jCount++) {
      if (aValue[iCount] < aValue[jCount]) {
        iTemporal = aValue[iCount];

        for (kCount = iCount; kCount >= jCount + 1; kCount--) {
          aValue[kCount] = aValue[kCount - 1];
        }

        aValue[jCount] = iTemporal;
        break;
      }
    }
  }

  // Affichage des valeurs
  for (mCount = 0; mCount < iNbrValue; mCount++) {
    if (mCount < iNbrValue - 1) {
      sArray += aValue[mCount] + ", ";
    } else {
      sArray += aValue[mCount];
    }
  }

  // Test de la consécutivité
  if (iNbrValue === 1) {
    sAnswer.innerHTML =
      "Voici le tableau avec votre valeur :<br><br>  aValue[" +
      sArray +
      "]<br><br> Et cette valeur est seule, donc elle NE peut PAS être consécutive.";
  } else {
    for (nCount = 0; nCount < iNbrValue - 1; nCount++) {
      if (aValue[nCount + 1] === aValue[nCount] + 1) {
        bConsecutive = true;
      } else {
        bConsecutive = false;
        break;
      }
    }

    if (bConsecutive === true) {
      sAnswer.innerHTML =
        "Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" +
        sArray +
        "]<br><br> Et ses valeurs sont consécutives.";
    } else {
      sAnswer.innerHTML =
        "Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" +
        sArray +
        "]<br><br> Et ses valeurs NE sont PAS consécutives.";
    }
  }

  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display =
    "none";
  document.getElementById("btn_reset_nbr_value").style.display = "none";
  document.getElementById("input_nbr_value").innerHTML = "";
  document.getElementById("iNbrValue").value = "";
  document.getElementById("btn_nbr_value").style.display = "inline";
}

function nbrConsecutiveOrNotJqr() {
  var iNbrValue = Number($("#iNbrValue").val());
  var sAnswer = $("#answer_formulary_medium");
  var aValue = [];
  var sArray = "";
  var bConsecutive = false;
  var iNbr, iTemporal, iCount, jCount, kCount, mCount, nCount;

  // Attribution et triage des valeurs
  for (iCount = 0; iCount < iNbrValue; iCount++) {
    iNbr = Number($("#iNbr_" + (iCount + 1)).val());

    aValue[iCount] = iNbr;

    for (jCount = 0; jCount <= iCount - 1; jCount++) {
      if (aValue[iCount] < aValue[jCount]) {
        iTemporal = aValue[iCount];

        for (kCount = iCount; kCount >= jCount + 1; kCount--) {
          aValue[kCount] = aValue[kCount - 1];
        }

        aValue[jCount] = iTemporal;
        break;
      }
    }
  }

  // Affichage des valeurs
  for (mCount = 0; mCount < iNbrValue; mCount++) {
    if (mCount < iNbrValue - 1) {
      sArray += aValue[mCount] + ", ";
    } else {
      sArray += aValue[mCount];
    }
  }

  // Test de la consécutivité
  if (iNbrValue === 1) {
    sAnswer.html(
      "<b>Voici le tableau avec votre valeur :<br><br>  aValue[" +
        sArray +
        "]<br><br> Et cette valeur est seule, donc elle NE peut PAS être consécutive.</b>"
    );
  } else {
    for (nCount = 0; nCount < iNbrValue - 1; nCount++) {
      if (aValue[nCount + 1] === aValue[nCount] + 1) {
        bConsecutive = true;
      } else {
        bConsecutive = false;
        break;
      }
    }

    if (bConsecutive === true) {
      sAnswer.html(
        "<b>Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" +
          sArray +
          "]<br><br> Et ses valeurs sont consécutives.</b>"
      );
    } else {
      sAnswer.html(
        "<b>Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" +
          sArray +
          "]<br><br> Et ses valeurs NE sont PAS consécutives.</b>"
      );
    }
  }

  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();
}

/* EXERCICE 2 */

/* EXERCICE 3 */

/* EXERCICE 4 */

function deleteIndexJs() {
  var iIndexDeleted = Number(document.getElementById("iIndexDeleted").value);
  var sAnswer = document.getElementById("answer_formulary");
  var aValue = [12, 8, 4, 45, 64, 9, 2];
  var iNbrValue = 7;
  var sArray = "";
  var iCount;

  if (iIndexDeleted >= 0 && iIndexDeleted < iNbrValue) {
    aValue.splice(iIndexDeleted, 1);

    for (iCount = 0; iCount < iNbrValue - 1; iCount++) {
      if (iCount < iNbrValue - 2) {
        sArray += aValue[iCount] + ", ";
      } else {
        sArray += aValue[iCount];
      }
    }

    sAnswer.innerHTML =
      "Voici le tableau avec l'index N°" +
      iIndexDeleted +
      " qui a été supprimé :<br><br>  aValue[" +
      sArray +
      "]<br><br> En partant du même tableau de départ avec ses 7 valeurs, vous pouvez tester de supprimer un index différent dans un autre language ou dans le même, en l'indiquant à côté.";
  } else {
    sAnswer.innerHTML =
      "Veuillez saisir un index compris entre 0 et " +
      (iNbrValue - 1) +
      " inclus, merci.";
  }

  document.getElementById("iIndexDeleted").value = "";
}

function deleteIndexJqr() {
  var iIndexDeleted = Number($("#iIndexDeleted").val());
  var sAnswer = $("#answer_formulary");
  var aValue = [12, 8, 4, 45, 64, 9, 2];
  var iNbrValue = 7;
  var sArray = "";
  var iCount;

  if (iIndexDeleted >= 0 && iIndexDeleted < iNbrValue) {
    aValue.splice(iIndexDeleted, 1);

    for (iCount = 0; iCount < iNbrValue - 1; iCount++) {
      if (iCount < iNbrValue - 2) {
        sArray += aValue[iCount] + ", ";
      } else {
        sArray += aValue[iCount];
      }
    }

    sAnswer.html(
      "<b>Voici le tableau avec l'index N°" +
        iIndexDeleted +
        " qui a été supprimé :<br><br>  aValue[" +
        sArray +
        "]<br><br> En partant du même tableau de départ avec ses 7 valeurs, vous pouvez tester de supprimer un index différent dans un autre language ou dans le même, en l'indiquant à côté.</b>"
    );
  } else {
    sAnswer.html(
      "<b>Veuillez saisir un index compris entre 0 et " +
        (iNbrValue - 1) +
        " inclus, merci.</b>"
    );
  }

  $("#iIndexDeleted").val("");
}

/* EXERCICE 5 */
var aOfWords = [];

function searchDicoJs() {
  var sWordSearched = document.getElementById("sWordSearched").value;
  var sAnswer = document.getElementById("answer_formulary");
  var iLimitMin = 0;
  var iLimitMax = aOfWords.length;
  var iNbrLoop = 0;
  var bFindedWord = false;
  var iReferenceIndex;

  sWordSearched = sWordSearched.toLowerCase();

  while (bFindedWord === false && iLimitMax - iLimitMin > 1) {
    iReferenceIndex = Math.floor((iLimitMin + iLimitMax) / 2);

    if (sWordSearched < aOfWords[iReferenceIndex]) {
      iLimitMax = iReferenceIndex;
    } else if (sWordSearched > aOfWords[iReferenceIndex]) {
      iLimitMin = iReferenceIndex;
    } else {
      bFindedWord = true;
    }

    iNbrLoop++;
  }

  if (bFindedWord === true) {
    sAnswer.innerHTML =
      "Votre mot '" +
      sWordSearched +
      "' se trouve à l'index N°" +
      iReferenceIndex +
      " de ce dictionnaire.<br><br> Et il a été trouvé en " +
      iNbrLoop +
      " tour de boucle.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.";
  } else {
    sAnswer.innerHTML =
      "Votre mot '" +
      sWordSearched +
      "' n'est pas dans ce dictionnaire.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.";
  }

  document.getElementById("sWordSearched").value = "";
}

var aOfWords = [];

function searchDicoJqr() {
  var sWordSearched = $("#sWordSearched").val();
  var sAnswer = $("#answer_formulary");
  var iLimitMin = 0;
  var iLimitMax = aOfWords.length;
  var iNbrLoop = 0;
  var bFindedWord = false;
  var iReferenceIndex;

  sWordSearched = sWordSearched.toLowerCase();

  while (bFindedWord === false && iLimitMax - iLimitMin > 1) {
    iReferenceIndex = Math.floor((iLimitMin + iLimitMax) / 2);

    if (sWordSearched < aOfWords[iReferenceIndex]) {
      iLimitMax = iReferenceIndex;
    } else if (sWordSearched > aOfWords[iReferenceIndex]) {
      iLimitMin = iReferenceIndex;
    } else {
      bFindedWord = true;
    }

    iNbrLoop++;
  }

  if (bFindedWord === true) {
    sAnswer.html(
      "<b>Votre mot '" +
        sWordSearched +
        "' se trouve à l'index N°" +
        iReferenceIndex +
        " de ce dictionnaire.<br><br> Et il a été trouvé en " +
        iNbrLoop +
        " tour de boucle.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.</b>"
    );
  } else {
    sAnswer.html(
      "<b>Votre mot '" +
        sWordSearched +
        "' n'est pas dans ce dictionnaire.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.</b>"
    );
  }

  $("#sWordSearched").val("");
}

/* EXERCICE 6 */

/* EXERCICE 7 */

function createThirdArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aValue_1 = [1, 7, 13, 15, 19, 31, 35, 51];
  var aValue_2 = [3, 5, 7, 9, 11, 15, 17, 31, 33, 37, 39, 51];
  var aValue_3 = [];
  var index_aValue_1 = 0;
  var index_aValue_2 = 0;
  var index_aValue_3 = 0;
  var sArray = "";

  while (index_aValue_3 < aValue_1.length + aValue_2.length) {
    while (
      index_aValue_1 < aValue_1.length &&
      (index_aValue_2 === aValue_2.length ||
        aValue_1[index_aValue_1] <= aValue_2[index_aValue_2])
    ) {
      aValue_3[index_aValue_3] = aValue_1[index_aValue_1];
      index_aValue_3++;
      index_aValue_1++;
    }
    while (
      index_aValue_2 < aValue_2.length &&
      (index_aValue_1 === aValue_1.length ||
        aValue_1[index_aValue_1] > aValue_2[index_aValue_2])
    ) {
      aValue_3[index_aValue_3] = aValue_2[index_aValue_2];
      index_aValue_3++;
      index_aValue_2++;
    }
  }

  for (iCount = 0; iCount < aValue_3.length; iCount++) {
    if (iCount < aValue_3.length - 1) {
      sArray += aValue_3[iCount] + ", ";
    } else {
      sArray += aValue_3[iCount];
    }
  }

  sAnswer.innerHTML =
    "Voici le 3ème tableau créé avec ses " +
    aValue_3.length +
    " valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue_3 = [" +
    sArray +
    "]";
}

function createThirdArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aValue_1 = [1, 7, 13, 15, 19, 31, 35, 51];
  var aValue_2 = [3, 5, 7, 9, 11, 15, 17, 31, 33, 37, 39, 51];
  var aValue_3 = [];
  var index_aValue_1 = 0;
  var index_aValue_2 = 0;
  var index_aValue_3 = 0;
  var sArray = "";

  while (index_aValue_3 < aValue_1.length + aValue_2.length) {
    while (
      index_aValue_1 < aValue_1.length &&
      (index_aValue_2 === aValue_2.length ||
        aValue_1[index_aValue_1] <= aValue_2[index_aValue_2])
    ) {
      aValue_3[index_aValue_3] = aValue_1[index_aValue_1];
      index_aValue_3++;
      index_aValue_1++;
    }
    while (
      index_aValue_2 < aValue_2.length &&
      (index_aValue_1 === aValue_1.length ||
        aValue_1[index_aValue_1] > aValue_2[index_aValue_2])
    ) {
      aValue_3[index_aValue_3] = aValue_2[index_aValue_2];
      index_aValue_3++;
      index_aValue_2++;
    }
  }

  for (iCount = 0; iCount < aValue_3.length; iCount++) {
    if (iCount < aValue_3.length - 1) {
      sArray += aValue_3[iCount] + ", ";
    } else {
      sArray += aValue_3[iCount];
    }
  }

  sAnswer.html(
    "<b>Voici le 3ème tableau créé avec ses " +
      aValue_3.length +
      " valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue_3 = [" +
      sArray +
      "]</b>"
  );
}

/* EXERCICE 8 */

/* Ces éléments sont à ajouter à la fin des fonctions, pour les exercices où l'on a besoin de faire choisir à l'utilisateur le nombre de valeur à indiquer

// A ajouter à la fin de la fonction de l'exercice, en Javascript
  document.getElementById("choice_nbr_random_create_input").style.display =
    "none";
  document.getElementById("btn_nbr_random_create_input").style.display = "none";
  document.getElementById("formulary_create_input").style.display = "none";
  document.getElementById("test_codes_create_input").style.display = "none";
  document.getElementById("down_test_codes_create_input").style.display = "none";
  document.getElementById("btn_reset_nbr_value").style.display = "none";
  document.getElementById("input_nbr_value").innerHTML = "";
  document.getElementById("iNbrValue").value = "";
  document.getElementById("btn_nbr_value").style.display = "inline";

// A ajouter à la fin de la fonction de l'exercice, en JQuery
  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();

*/
