/* SAISON 8 - Tous les Exercices */

/* EXERCICE 1 */

function emptyZeroJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aArray = [];
  var sArray_1 = "";
  var sArray_2 = "";
  var iCount, jCount;

  for (iCount = 0; iCount < 6; iCount++) {
    aArray[iCount] = [];

    sArray_2 = "";
    for (jCount = 0; jCount < 13; jCount++) {
      aArray[iCount][jCount] = 0;

      if (jCount < 12) {
        sArray_2 += aArray[iCount][jCount] + ", ";
      } else {
        sArray_2 += aArray[iCount][jCount] + "]<br>";
      }
    }

    sArray_1 += "aArray[" + iCount + "] =  [" + sArray_2;
  }

  sAnswer.innerHTML =
    "Voici le tableau à 2 dimensions, de 6 sur 13, une fois rempli de 0 pour chaque index :<br><br>" +
    sArray_1;
}

function emptyZeroJqr() {
  var sAnswer = $("#answer_formulary");
  var aArray = [];
  var sArray_1 = "";
  var sArray_2 = "";
  var iCount, jCount;

  for (iCount = 0; iCount < 6; iCount++) {
    aArray[iCount] = [];

    sArray_2 = "";
    for (jCount = 0; jCount < 13; jCount++) {
      aArray[iCount][jCount] = 0;

      if (jCount < 12) {
        sArray_2 += aArray[iCount][jCount] + ", ";
      } else {
        sArray_2 += aArray[iCount][jCount] + "]<br>";
      }
    }

    sArray_1 += "aArray[" + iCount + "] =  [" + sArray_2;
  }

  sAnswer.html(
    "<b>Voici le tableau à 2 dimensions, de 6 sur 13, une fois rempli de 0 pour chaque index :<br><br>" +
      sArray_1 +
      "</b>"
  );
}

/* EXERCICE 2 */

function sequenceArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aX = [];
  var iVal = 1;
  var iCount, jCount;

  for (iCount = 0; iCount < 2; iCount++) {
    aX[iCount] = [];

    for (jCount = 0; jCount < 3; jCount++) {
      aX[iCount][jCount] = iVal;
      iVal++;
    }
  }

  for (iCount = 0; iCount < 2; iCount++) {
    for (jCount = 0; jCount < 3; jCount++) {
      sAnswer.innerHTML += aX[iCount][jCount];
    }
  }
}

function sequenceArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aX = [];
  var iVal = 1;
  var sArray = "";
  var iCount, jCount;

  for (iCount = 0; iCount < 2; iCount++) {
    aX[iCount] = [];

    for (jCount = 0; jCount < 3; jCount++) {
      aX[iCount][jCount] = iVal;
      iVal++;
    }
  }

  for (iCount = 0; iCount < 2; iCount++) {
    for (jCount = 0; jCount < 3; jCount++) {
      sArray += aX[iCount][jCount];
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 3 */

function sequenceInDisorderArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aX = [];
  var iVal = 1;
  var iCount, jCount;

  for (iCount = 0; iCount < 2; iCount++) {
    aX[iCount] = [];

    for (jCount = 0; jCount < 3; jCount++) {
      aX[iCount][jCount] = iVal;
      iVal++;
    }
  }

  for (jCount = 0; jCount < 3; jCount++) {
    for (iCount = 0; iCount < 2; iCount++) {
      sAnswer.innerHTML += aX[iCount][jCount];
    }
  }
}

function sequenceInDisorderArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aX = [];
  var iVal = 1;
  var sArray = "";
  var iCount, jCount;

  for (iCount = 0; iCount < 2; iCount++) {
    aX[iCount] = [];

    for (jCount = 0; jCount < 3; jCount++) {
      aX[iCount][jCount] = iVal;
      iVal++;
    }
  }

  for (jCount = 0; jCount < 3; jCount++) {
    for (iCount = 0; iCount < 2; iCount++) {
      sArray += aX[iCount][jCount];
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 4 */

function summingArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aT = [];
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = kCount + mCount;
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sAnswer.innerHTML += aT[kCount][mCount];
    }
  }
}

function summingArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aT = [];
  var sArray = "";
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = kCount + mCount;
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sArray += aT[kCount][mCount];
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 5.A */

function otherSequenceArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aT = [];
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = 2 * kCount + (mCount + 1);
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sAnswer.innerHTML += aT[kCount][mCount];
    }
  }
}

function otherSequenceArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aT = [];
  var sArray = "";
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = 2 * kCount + (mCount + 1);
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sArray += aT[kCount][mCount];
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 5.B */

function intersectingSequencyArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aT = [];
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = kCount + 1 + 4 * mCount;
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sAnswer.innerHTML += aT[kCount][mCount];
    }
  }
}

function intersectingSequencyArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aT = [];
  var sArray = "";
  var kCount, mCount;

  for (kCount = 0; kCount < 4; kCount++) {
    aT[kCount] = [];

    for (mCount = 0; mCount < 2; mCount++) {
      aT[kCount][mCount] = kCount + 1 + 4 * mCount;
    }
  }

  for (kCount = 0; kCount < 4; kCount++) {
    for (mCount = 0; mCount < 2; mCount++) {
      sArray += aT[kCount][mCount];
    }
  }

  sAnswer.html("<b>" + sArray + "</b>");
}

/* EXERCICE 6 */

function valueMaxArrayJs() {
  var sAnswer = document.getElementById("answer_formulary");
  var aT = [];
  var iValueMax, iIndex_1_Max, iIndex_2_Max, iCount, jCount;

  aT[0] = [21, 56, 8, 654, 0, 684, 34, 67];
  aT[1] = [62, 689, 76, 8, 7, 5, 55, 557];
  aT[2] = [85, 554, 22, 867, 83, 837, 785, 3];
  aT[3] = [35, 87, 678, 23, 58, 0, 85, 36];
  aT[4] = [786, 38, 783, 38, 95, 89, 87, 287];
  aT[5] = [76, 976, 876, 88, 676, 76, 97, 19];
  aT[6] = [29, 15, 321, 932, 32, 15, 69, 972];
  aT[7] = [1, 152, 5, 22, 5432, 684, 178, 71];
  aT[8] = [37, 875, 85, 76, 86, 48, 21, 782];
  aT[9] = [35, 414, 328, 811, 857, 854, 2, 157];
  aT[10] = [383, 586, 18, 188, 81, 851, 159, 6];
  aT[11] = [196, 62, 6, 52, 57, 5, 4, 3];

  for (iCount = 0; iCount < 12; iCount++) {
    for (jCount = 0; jCount < 8; jCount++) {
      if ((iCount === 0 && jCount === 0) || iValueMax < aT[iCount][jCount]) {
        iValueMax = aT[iCount][jCount];
        iIndex_1_Max = iCount;
        iIndex_2_Max = jCount;
      }
    }
  }

  sAnswer.innerHTML =
    "Voici la plus grande valeur contenu dans ce tableau aT :<br><br>" +
    iValueMax +
    "<br><br>Et elle se trouve aux cordonnées :<br><br> aT[" +
    iIndex_1_Max +
    "][" +
    iIndex_2_Max +
    "]";
}

function valueMaxArrayJqr() {
  var sAnswer = $("#answer_formulary");
  var aT = [];
  var iValueMax, iIndex_1_Max, iIndex_2_Max, iCount, jCount;

  aT[0] = [21, 56, 8, 654, 0, 684, 34, 67];
  aT[1] = [62, 689, 76, 8, 7, 5, 55, 557];
  aT[2] = [85, 554, 22, 867, 83, 837, 785, 3];
  aT[3] = [35, 87, 678, 23, 58, 0, 85, 36];
  aT[4] = [786, 38, 783, 38, 95, 89, 87, 287];
  aT[5] = [76, 976, 876, 88, 676, 76, 97, 19];
  aT[6] = [29, 15, 321, 932, 32, 15, 69, 972];
  aT[7] = [1, 152, 5, 22, 5432, 684, 178, 71];
  aT[8] = [37, 875, 85, 76, 86, 48, 21, 782];
  aT[9] = [35, 414, 328, 811, 857, 854, 2, 157];
  aT[10] = [383, 586, 18, 188, 81, 851, 159, 6];
  aT[11] = [196, 62, 6, 52, 57, 5, 4, 3];

  for (iCount = 0; iCount < 12; iCount++) {
    for (jCount = 0; jCount < 8; jCount++) {
      if ((iCount === 0 && jCount === 0) || iValueMax < aT[iCount][jCount]) {
        iValueMax = aT[iCount][jCount];
        iIndex_1_Max = iCount;
        iIndex_2_Max = jCount;
      }
    }
  }

  sAnswer.html(
    "<b>Voici la plus grande valeur contenu dans ce tableau aT :<br><br>" +
      iValueMax +
      "<br><br>Et elle se trouve aux cordonnées :<br><br> aT[" +
      iIndex_1_Max +
      "][" +
      iIndex_2_Max +
      "]</b>"
  );
}

/* EXERCICE 7 */
