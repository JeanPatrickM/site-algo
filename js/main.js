/* Fixer la Nav en haut de la page après l'avoir faite défiler jusqu'à sa hauteur */

$(function () {
  // On recupere la position du bloc par rapport au haut du site
  var position_top_diminue = $("#nav_fixed").offset().top;

  //Au scroll dans la fenetre on déclenche la fonction
  $(window).scroll(function () {
    //si on a defile de plus de 190px du haut vers le bas, c'est à dire la hauteur de mon header + ses marges
    if ($(this).scrollTop() > position_top_diminue) {
      $("#nav_fixed").addClass("fixed_naviguation");
    } else {
      $("#nav_fixed").removeClass("fixed_naviguation");
    }
  });
});

/* Pour la saison 3, faire apparaître et disparaître les exercices de Javascript et Jquery */

function displayOrHideJsS3() {
  var sToggJsS3 = document.getElementById("answer_js_s3");
  var sToggJqrS3 = document.getElementById("answer_jqr_s3");
  var sBtnJsS3 = document.getElementById("btn_js_s3");
  var sBtnJqrS3 = document.getElementById("btn_jqr_s3");

  if (sToggJsS3.style.display == "none") {
    sToggJsS3.style.display = "block";
    sToggJqrS3.style.display = "none";
    sBtnJsS3.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqrS3.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  } else {
    sToggJsS3.style.display = "block";
    sToggJqrS3.style.display = "none";
    sBtnJsS3.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqrS3.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  }
}

function displayOrHideJqrS3() {
  var sToggJsS3 = document.getElementById("answer_js_s3");
  var sToggJqrS3 = document.getElementById("answer_jqr_s3");
  var sBtnJsS3 = document.getElementById("btn_js_s3");
  var sBtnJqrS3 = document.getElementById("btn_jqr_s3");

  if (sToggJqrS3.style.display == "none") {
    sToggJqrS3.style.display = "block";
    sToggJsS3.style.display = "none";
    sBtnJqrS3.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJsS3.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  } else {
    sToggJqrS3.style.display = "block";
    sToggJsS3.style.display = "none";
    sBtnJqrS3.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJsS3.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  }
}

/* A partir de la saison 4, faire apparaître et disparaître les exercices de Javascript, Jquery et PHP */

function displayOrHideJs() {
  var sToggJs = document.getElementById("answer_js");
  var sToggJqr = document.getElementById("answer_jqr");
  var sToggPhp = document.getElementById("answer_php");
  var sBtnJs = document.getElementById("btn_js");
  var sBtnJqr = document.getElementById("btn_jqr");
  var sBtnPhp = document.getElementById("btn_php");
  var sBtnJs = document.getElementById("btn_js");
  var sBtnJqr = document.getElementById("btn_jqr");
  var sBtnPhp = document.getElementById("btn_php");

  if (sToggJs.style.display == "none") {
    sToggJs.style.display = "block";
    sToggJqr.style.display = "none";
    sToggPhp.style.display = "none";
    sBtnJs.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqr.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnPhp.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  } else {
    sToggJs.style.display = "block";
    sToggJqr.style.display = "none";
    sToggPhp.style.display = "none";
    sBtnJs.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqr.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnPhp.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  }
}

function displayOrHideJqr() {
  var sToggJs = document.getElementById("answer_js");
  var sToggJqr = document.getElementById("answer_jqr");
  var sToggPhp = document.getElementById("answer_php");
  var sBtnJs = document.getElementById("btn_js");
  var sBtnJqr = document.getElementById("btn_jqr");
  var sBtnPhp = document.getElementById("btn_php");

  if (sToggJqr.style.display == "none") {
    sToggJqr.style.display = "block";
    sToggJs.style.display = "none";
    sToggPhp.style.display = "none";
    sBtnJqr.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJs.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnPhp.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  } else {
    sToggJqr.style.display = "block";
    sToggJs.style.display = "none";
    sToggPhp.style.display = "none";
    sBtnJqr.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJs.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnPhp.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  }
}

function displayOrHidePhp() {
  var sToggJs = document.getElementById("answer_js");
  var sToggJqr = document.getElementById("answer_jqr");
  var sToggPhp = document.getElementById("answer_php");
  var sBtnJs = document.getElementById("btn_js");
  var sBtnJqr = document.getElementById("btn_jqr");
  var sBtnPhp = document.getElementById("btn_php");

  if (sToggPhp.style.display == "none") {
    sToggPhp.style.display = "block";
    sToggJqr.style.display = "none";
    sToggJs.style.display = "none";
    sBtnPhp.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqr.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnJs.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  } else {
    sToggPhp.style.display = "block";
    sToggJqr.style.display = "none";
    sToggJs.style.display = "none";
    sBtnPhp.style.background =
      "linear-gradient(to top, #878a82 0%, #a3a59f 40%, #b8b9b5 100%)";
    sBtnJqr.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
    sBtnJs.style.background =
      "linear-gradient(to top, #4a4c47 0%, #80837a 40%,#94978e 100%)";
  }
}

/* Ces 3 fonctions servent uniquement pour les exercices où l'on a besoin de faire choisir à l'utilisateur le nombre de valeur à indiquer */

// Cette fonction sert pour choisir le nombre de valeur à indiquer
function createNbrValue() {
  var iNbrValue = Number($("#iNbrValue").val());
  var sInputNbrValue = "";

  if (iNbrValue >= 1) {
    $("#input_nbr_value").html("");
    $("#answer_formulary_medium").html("");
    $("#btn_nbr_value").hide();

    // Proposer de générer les nombres au hasard
    if (iNbrValue >= 2) {
      $("#choice_nbr_random_create_input").html(
        "<em>Vous pouvez entrer les valeurs manuellement, ou les générer au hasard</em>"
      );
      $("#choice_nbr_random_create_input").show();
      $("#btn_nbr_random_create_input").show();
    } else {
      $("#choice_nbr_random_create_input").hide();
      $("#choice_nbr_random_create_input").html("");
    }

    // Création des input pour entrer les valeurs
    for (iCount = 1; iCount <= iNbrValue; iCount++) {
      sInputNbrValue +=
        '<input type="text" class="first_input_formulary" id="iNbr_' +
        iCount +
        '" name="iNbr_' +
        iCount +
        '" placeholder="Indiquez le nombre N°' +
        iCount +
        '">';
    }

    $("#input_nbr_value").html(sInputNbrValue);
    $("#formulary_create_input").css("display", "flex");
    $("#test_codes_create_input").css("display", "flex");
    $("#btn_reset_nbr_value").show();
    if (iNbrValue >= 20) {
      $("#down_test_codes_create_input").css("display", "flex");
    }
  } else {
    $("#choice_nbr_random_create_input").html(
      '<strong>Le nombre de valeur doit être au minimum de <span style="color: red;">1</span>, veuillez indiquer un nouveau nombre, merci.</strong>'
    );
    $("#choice_nbr_random_create_input").show();
    $("#iNbrValue").val("");
  }
}
// Cette fonction sert pour créer 100 champs de valeur à indiquer (Comme pour l'exo 6.17)
function create_100_Value() {
  var sInputNbrValue = "";

  $("#input_nbr_value").html("");
  $("#answer_formulary_medium").html("");
  $("#btn_nbr_value_specified").hide();

  $("#choice_nbr_random_create_input").html(
    "<em>Vous pouvez entrer les valeurs manuellement, ou les générer au hasard</em>"
  );
  $("#choice_nbr_random_create_input").show();
  $("#btn_nbr_random_create_input").show();

  for (iCount = 1; iCount <= 100; iCount++) {
    sInputNbrValue +=
      '<input type="text" class="first_input_formulary" id="iNbr_' +
      iCount +
      '" name="iNbr_' +
      iCount +
      '" placeholder="Indiquez le nombre N°' +
      iCount +
      '">';
  }

  $("#input_nbr_value").html(sInputNbrValue);
  $("#formulary_create_input").css("display", "flex");
  $("#test_codes_create_input").css("display", "flex");
  $("#down_test_codes_create_input").css("display", "flex");
}

// Cette fonction sert pour générer des nombres au hasard de -100 à 100
function nbrRandomCreateInput() {
  var iNbrValue = Number($("#iNbrValue").val());
  var iNbrRandom;
  var sInputNbrRandom = "";

  for (iCount = 1; iCount <= iNbrValue; iCount++) {
    iNbrRandom = Math.floor(Math.random() * (100 - -100 + 1)) + -100;
    sInputNbrRandom +=
      '<input type="text" class="first_input_formulary" id="iNbr_' +
      iCount +
      '" name="iNbr_' +
      iCount +
      '" value="' +
      iNbrRandom +
      '" placeholder="Indiquez le nombre N°' +
      iCount +
      '">';
  }

  $("#input_nbr_value").html(sInputNbrRandom);
}
// Cette fonction sert pour générer 100 nombres au hasard de -100 à 100  (Comme pour l'exo 6.17)
function nbrRandomCreate_100_Input() {
  var iNbrRandom;
  var sInputNbrRandom = "";

  for (iCount = 1; iCount <= 100; iCount++) {
    iNbrRandom = Math.floor(Math.random() * (100 - -100 + 1)) + -100;
    sInputNbrRandom +=
      '<input type="text" class="first_input_formulary" id="iNbr_' +
      iCount +
      '" name="iNbr_' +
      iCount +
      '" value="' +
      iNbrRandom +
      '" placeholder="Indiquez le nombre N°' +
      iCount +
      '">';
  }

  $("#input_nbr_value").html(sInputNbrRandom);
}

// Cette fonction sert si l'iutilisateur veut modifier le nombre de valeur, avant d'avoir fait un test dans l'un des languages.
// Mais sinon ce sera automatiquement possible après chaque test de language
function resetNbrValue() {
  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#test_codes_create_input").css("display", "none");
  $("#down_test_codes_create_input").css("display", "none");
  $("#btn_reset_nbr_value").hide();
  $("#input_nbr_value").html("");
  $("#iNbrValue").val("");
  $("#btn_nbr_value").show();
}

/* Ces 4 fonctions servent uniquement pour les exercices où l'on veut faire créer à l'utilisateur, 2 tableaux comme il le souhaite (notammant pour l'exo 6.11) */
/*Cependant, avant de les mettre en fonction il faut que je trouve pourquoi le 2ème tableau prend pour valeur "NaN" pour chacun de ses index, alors qu'il a le même code que le 1er tableau, et que celui-ci prend bien les valeurs saisies...

// Cette fonction sert pour choisir le nombre de valeur à indiquer pour chaque tableau
function createInputArrays() {
  var iNbrValueArray_1 = Number($("#iNbrValueArray_1").val());
  var iNbrValueArray_2 = Number($("#iNbrValueArray_2").val());
  var sInputArray_1 = "";
  var sInputArray_2 = "";

  if (iNbrValueArray_1 >= 1 && iNbrValueArray_2 >= 1) {
    $("#input_nbr_value_array_1").html("");
    $("#input_nbr_value_array_2").html("");
    $("#answer_formulary_medium").html("");

    $("#btn_nbr_value").hide();
    $("#test_codes").hide();
    $("#choice_nbr_random_create_input").html(
      "<em>Vous pouvez entrer les valeurs manuellement, ou les générer au hasard</em>"
    );
    $("#choice_nbr_random_create_input").show();
    $("#btn_nbr_random_create_input").show();

    for (iCount = 1; iCount <= iNbrValueArray_1; iCount++) {
      sInputArray_1 +=
        '<input type="text" class="first_input_formulary" id="iNbr_' +
        iCount +
        '" name="iNbr_' +
        iCount +
        '" placeholder="Indiquez le nombre N°' +
        iCount +
        '">';
    }

    for (jCount = 1; jCount <= iNbrValueArray_2; jCount++) {
      sInputArray_2 +=
        '<input type="text" class="first_input_formulary" id="iNbr_' +
        jCount +
        '" name="iNbr_' +
        jCount +
        '" placeholder="Indiquez le nombre N°' +
        jCount +
        '">';
    }

    $("#input_nbr_value_array_1").html(sInputArray_1);
    $("#input_nbr_value_array_2").html(sInputArray_2);
    $("#formulary_create_input").css("display", "flex");
    $("#create_arrays").show();
    $("#btn_create_arrays").show();
  } else {
    $("#choice_nbr_random_create_input").html(
      '<strong>Le nombre de valeur doit être au minimum de <span style="color: red;">1</span> pour les 2 tableaux, veuillez indiquer des nombres corrects, merci.</strong>'
    );
    $("#choice_nbr_random_create_input").show();
    $("#iNbrValueArray_1").val("");
    $("#iNbrValueArray_2").val("");
  }
}

// Cette fonction sert pour générer des nombres au hasard pour chaque tableau
function nbrRandomCreateArrays() {
  var iNbrValueArray_1 = Number($("#iNbrValueArray_1").val());
  var iNbrValueArray_2 = Number($("#iNbrValueArray_2").val());
  var iNbrRandom;
  var sInputNbrRandomArray_1 = "";
  var sInputNbrRandomArray_2 = "";

  for (iCount = 1; iCount <= iNbrValueArray_1; iCount++) {
    iNbrRandom = Math.floor(Math.random() * (100 - -100 + 1)) + -100;
    sInputNbrRandomArray_1 +=
      '<input type="text" class="first_input_formulary" id="iَArray_1_Nbr_' +
      iCount +
      '" name="iArray_1_Nbr_' +
      iCount +
      '" value="' +
      iNbrRandom +
      '" placeholder="Indiquez le nombre N°' +
      iCount +
      '">';
  }

  for (jCount = 1; jCount <= iNbrValueArray_2; jCount++) {
    iNbrRandom = Math.floor(Math.random() * (100 - -100 + 1)) + -100;
    sInputNbrRandomArray_2 +=
      '<input type="text" class="first_input_formulary" id="iArray_2_Nbr_' +
      jCount +
      '" name="iArray_2_Nbr_' +
      jCount +
      '" value="' +
      iNbrRandom +
      '" placeholder="Indiquez le nombre N°' +
      jCount +
      '">';
  }

  $("#input_nbr_value_array_1").html(sInputNbrRandomArray_1);
  $("#input_nbr_value_array_2").html(sInputNbrRandomArray_2);
}

// Cette fonction sert à créer les tableaux
function createArrays() {
  var iNbrValueArray_1 = Number($("#iNbrValueArray_1").val());
  var iNbrValueArray_2 = Number($("#iNbrValueArray_2").val());
  var jCount;
  var iNbr;
  var aArray_1 = [];
  var aArray_2 = [];
  var sValuesArray_1 = "";
  var sValuesArray_2 = "";
  var sArray_1 = "";
  var sArray_2 = "";

  for (iCount = 1; iCount <= iNbrValueArray_1; iCount++) {
    iNbr = Number($("#iَArray_1_Nbr_" + iCount).val());
    aArray_1[iCount - 1] = iNbr;

    if (iCount < iNbrValueArray_1) {
      sValuesArray_1 += aArray_1[iCount - 1] + ", ";
    } else {
      sValuesArray_1 += aArray_1[iCount - 1];
    }
  }

  for (jCount = 1; jCount <= iNbrValueArray_2; jCount++) {
    iNbr = Number($("#iَArray_2_Nbr_" + jCount).val());
    aArray_2[jCount - 1] = iNbr;

    if (jCount < iNbrValueArray_2) {
      sValuesArray_2 += aArray_2[jCount - 1] + ", ";
    } else {
      sValuesArray_2 += aArray_2[jCount - 1];
    }
  }

  sArray_1 = "Array_1 = [" + sValuesArray_1 + "]";
  sArray_2 = "Array_2 = [" + sValuesArray_2 + "]";

  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#create_arrays").hide();
  $("#btn_create_arrays").hide();
  $("#arrays_created").html(
    "<strong>Voici les 2 tableaux que vous avez créés pour l'exercice : <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
      sArray_1 +
      "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
      sArray_2 +
      "</strong>"
  );
  $("#arrays_created").css("display", "block");
  $("#test_codes").show();
}

// Cette fonction sert si l'iutilisateur veut modifier le nombre de valeur pour chacun des tableaux, avant d'avoir fait un test dans l'un des languages.
// Mais sinon ce sera automatiquement possible après chaque test de language
function resetArrays() {
  $("#choice_nbr_random_create_input").hide();
  $("#btn_nbr_random_create_input").hide();
  $("#formulary_create_input").css("display", "none");
  $("#arrays_created").css("display", "none");
  $("#arrays_created").html("");
  $("#test_codes").hide();
  $("#input_nbr_value").html("");
  $("#input_nbr_value_array_1").val("");
  $("#input_nbr_value_array_2").val("");
  $("#btn_nbr_value").show();
}
*/
