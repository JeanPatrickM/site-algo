<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $aValue_1 = [1, 7, 13, 15, 19, 31, 35, 51];
    $aValue_2 = [3, 5, 7, 9, 11, 15, 17, 31, 33, 37, 39, 51];
    $index_aValue_1 = 0;
    $index_aValue_2 = 0;
    $index_aValue_3 = 0;
    $sArray = "";

    while ($index_aValue_3 < (count($aValue_1) + count($aValue_2))) {
        while (
            $index_aValue_1 < count($aValue_1) &&
            ($index_aValue_2 === count($aValue_2) || $aValue_1[$index_aValue_1] <= $aValue_2[$index_aValue_2])
        ) {
            $aValue_3[$index_aValue_3] = $aValue_1[$index_aValue_1];
            $index_aValue_3++;
            $index_aValue_1++;
        }
        while (
            $index_aValue_2 < count($aValue_2) &&
            ($index_aValue_1 === count($aValue_1) || $aValue_1[$index_aValue_1] > $aValue_2[$index_aValue_2])
        ) {
            $aValue_3[$index_aValue_3] = $aValue_2[$index_aValue_2];
            $index_aValue_3++;
            $index_aValue_2++;
        }
    }

    for ($iCount = 0; $iCount < count($aValue_3); $iCount++) {
        if ($iCount < count($aValue_3) - 1) {
            $sArray .= $aValue_3[$iCount] . ", ";
        } else {
            $sArray .= $aValue_3[$iCount];
        }
    }

    $sAnswer =
        "<span style=\"color: grey;\">Voici le 3ème tableau créé avec ses " .
        count($aValue_3) .
        " valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue_3 = [" .
        $sArray .
        "]</span>";
}

require "exo_7.html";

?>

<!-- = "<span style=\"color: grey;\"> -->     <!-- </span>" -->