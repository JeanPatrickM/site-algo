<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $iIndexDeleted = (int)$_POST['iIndexDeleted'];
    $aValue = [12, 8, 4, 45, 64, 9, 2];
    $iNbrValue = 7;
    $sArray = "";

    if ($iIndexDeleted >= 0 && $iIndexDeleted <= $iNbrValue - 1) {
            array_splice($aValue,($iIndexDeleted),1);

            for ($iCount = 0; $iCount < $iNbrValue - 1; $iCount++) {
                if ($iCount < $iNbrValue - 2) {
                    $sArray .= $aValue[$iCount] . ", ";
                } else {
                    $sArray .= $aValue[$iCount];
                }
            }

        $sAnswer =
            "<span style=\"color: grey;\">Voici le tableau avec l'index N°" .
            $iIndexDeleted .
            " qui a été supprimé :<br><br>  aValue[" .
            $sArray .
            "]<br><br> En partant du même tableau de départ avec ses 7 valeurs, vous pouvez tester de supprimer un index différent dans un autre language ou dans le même, en l'indiquant à côté.</span>";
    } else {
        $sAnswer =
            "<span style=\"color: grey;\">Veuillez saisir un index compris entre 0 et " .
            ($iNbrValue - 1) .
            " inclus, merci.</span>";
    }
}

require "exo_4.html";

?>