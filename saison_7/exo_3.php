<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $iNbrValue = (int)$_POST['iNbrValue'];
    $sArray = "";
    $bConsecutive = false;

    // Attribution et triage des valeurs
    for ($iCount = 0; $iCount < $iNbrValue; $iCount++) {
        $iNbr = (int)$_POST['iNbr_' . ($iCount + 1)];

        $aValue[$iCount] = $iNbr;

        for ($jCount = 0; $jCount <= $iCount - 1; $jCount++) {
            if ($aValue[$iCount] < $aValue[$jCount]) {
                $iTemporal = $aValue[$iCount];

                for ($kCount = $iCount; $kCount >= $jCount + 1; $kCount--) {
                    $aValue[$kCount] = $aValue[$kCount - 1];
                }

                $aValue[$jCount] = $iTemporal;
                break;
            }
        }
    }

    // Affichage des valeurs
    for ($mCount = 0; $mCount < $iNbrValue; $mCount++) {
        if ($mCount < $iNbrValue - 1) {
            $sArray .= $aValue[$mCount] . ", ";
        } else {
            $sArray .= $aValue[$mCount];
        }
    }

    // Test de la consécutivité  
    for ($nCount = 0; $nCount < $iNbrValue - 1; $nCount++) {
        if ($aValue[$nCount + 1] === $aValue[$nCount] + 1) {
            $bConsecutive = true;
        } else {
            break;
        }
    }

    if ($bConsecutive === true) {
        $sAnswer =
            "<span style=\"color: grey;\">Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" .
            $sArray .
            "]<br><br> Et ses valeurs sont consécutives.</span>";
    } else {
        $sAnswer =
            "<span style=\"color: grey;\">Voici le tableau avec vos valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aValue[" .
            $sArray .
            "]<br><br> Et ses valeurs NE sont PAS consécutives.</span>";
        }
}

require "exo_3.html";

?>

<!-- = "<span style=\"color: grey;\"> -->     <!-- </span>" -->