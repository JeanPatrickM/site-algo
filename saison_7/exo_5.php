<?php

$sAnswer = "";

require "exo_5_mon_petit_dico.php";

// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $sWordSearched = $_POST['sWordSearched'];
    $iLimitMin = 0;
    $iLimitMax = count($aOfWords);
    $iNbrLoop = 0;
    $bFindedWord = false;

    $sWordSearched = mb_strtolower($sWordSearched);
    
    while ($bFindedWord === false && $iLimitMax - $iLimitMin > 1) {
        $iReferenceIndex = intval(($iLimitMin + $iLimitMax) / 2);

        if ($sWordSearched < $aOfWords[$iReferenceIndex]) {
            $iLimitMax = $iReferenceIndex;
        } else if ($sWordSearched > $aOfWords[$iReferenceIndex]) {
            $iLimitMin = $iReferenceIndex;
        } else {
            $bFindedWord = true;
        }

        $iNbrLoop++;
    }

    if ($bFindedWord === true) {
        $sAnswer =
            "<span style=\"color: grey;\">Votre mot '" .
                $sWordSearched .
                "' se trouve à l'index N°" .
                $iReferenceIndex .
                " de ce dictionnaire.<br><br> Et il a été trouvé en " .
                $iNbrLoop .
                " tour de boucle.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.</span>";
    } else {
        $sAnswer =
            "<span style=\"color: grey;\">Votre mot '" .
                $sWordSearched .
                "' n'est pas dans ce dictionnaire.<br><br> Vous pouvez chercher un autre mot en l'indiquant à côté.</span>";
        }
}

require "exo_5.html";

?>