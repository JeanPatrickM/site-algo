<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $sArray = "";
    $aVowels = ["a", "e", "i", "o", "u", "y"];

    for ($iCount = 0; $iCount <= 5; $iCount++) {
        $aArray[$iCount] = $aVowels[$iCount];
        $sArray = $sArray . "Index " . $iCount . " : " . $aArray[$iCount] . "<br>";
    }

    $sAnswer = "<span style=\"color: grey;\">" . $sArray . "</span>";
}

require "exo_2.html";

?>