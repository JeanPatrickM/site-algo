<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNbrValue = (int)$_POST['iNbrValue'];
  $iNegativeNbr = 0;
  $iPositiveNbr = 0;
  $sArray = "";

  for ($iCount = 1; $iCount <= $iNbrValue; $iCount++) {
    $iNbr = (int)$_POST['iNbr_' . $iCount];

    $aPositiveOrNegative[$iCount - 1] = $iNbr;

    if ($iNbr < 0) {
      $iNegativeNbr++;
    } else {
      $iPositiveNbr++;
    }

    if ($iNbrValue === 1 || $iNbrValue === $iCount) {
      $sArray .= $aPositiveOrNegative[$iCount - 1];
    } else {
      $sArray .= $aPositiveOrNegative[$iCount - 1] . ", ";
    }
  }

  $sAnswer = 
    "<span style=\"color: grey;\">Le tableau créé est donc :<br><br> aPositiveOrNegative = [" .
    $sArray .
    "] <br><br>Et il s'y trouve " .
    $iNegativeNbr .
    " valeur(s) négative(s), et " .
    $iPositiveNbr .
    " valeur(s) positive(s). <br><br>Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.</span>";
}

require "exo_8.html";

?>