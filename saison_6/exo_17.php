<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $sArray = "";

// Attribution et triage des valeurs
  for ($iCount = 0; $iCount < 100; $iCount++) {
    $iNbr = (int)$_POST['iNbr_' . ($iCount + 1)];

    $aHundredValue[$iCount] = $iNbr;

    for ($jCount = 0; $jCount <= $iCount - 1; $jCount++) {
      if ($aHundredValue[$iCount] < $aHundredValue[$jCount]) {
        $iTemporal = $aHundredValue[$iCount];

        for ($kCount = $iCount; $kCount >= $jCount + 1; $kCount--) {
          $aHundredValue[$kCount] = $aHundredValue[$kCount - 1];
        }

        $aHundredValue[$jCount] = $iTemporal;
        break;
      }
    }
  }

// Affichage des valeurs
  for ($mCount = 0; $mCount < 100; $mCount++) {
    if ($mCount < 99) {
      $sArray .= $aHundredValue[$mCount] . ", ";
    } else {
      $sArray .= $aHundredValue[$mCount];
    }
  }

  $sAnswer = 
    "<span style=\"color: grey;\">Voici le tableau avec les 100 valeurs triées au fur et à mesure, dans un ordre croissant :<br><br>  aHundredValue[" .
    $sArray .
    "]</span>";
}

require "exo_17.html";

?>