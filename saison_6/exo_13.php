<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iNbrValue = (int)$_POST['iNbrValue'];
  $sArray = "";

  for ($iCount = 0; $iCount <= $iNbrValue - 1; $iCount++) {
    $iNbr = (int)$_POST['iNbr_' . ($iCount + 1)];

    $aValue[$iCount] = $iNbr;

    if ($iCount === 0 || $aValue[$iCount] > $iMaxValue) {
      $iMaxValue = $aValue[$iCount];
      $iIndexMax = $iCount;
    }

    if ($iNbrValue === 1 || $iNbrValue === $iCount + 1) {
      $sArray .= $aValue[$iCount];
    } else {
      $sArray .= $aValue[$iCount] . ", ";
    }
  }

  $sAnswer = 
    "<span style=\"color: grey;\">Le tableau créé est donc :<br><br> aValue = [" .
    $sArray .
    "]<br><br> Sa plus grande valeur est " .
    $iMaxValue .
    ", elle se trouve à l'index N°" .
    $iIndexMax .
    ", et cette valeur est donc en position " . 
    ($iIndexMax + 1) . 
    " dans ce tableau.<br><br> Vous pouvez recommencer dans un autre language, ou dans le même, en choisissant un nouveau nombre de valeur.</span>";
}

require "exo_13.html";

?>