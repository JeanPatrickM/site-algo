<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $aArray_1 = [4, 8, 7, 12];
  $aArray_2 = [3, 6];
  $iNbrValueArray_1 = 4;
  $iNbrValueArray_2 = 2;
  $iResult = 0;
  $sArray = "";

  for ($iCount = 1; $iCount <= $iNbrValueArray_2; $iCount++) {
    for ($jCount = 1; $jCount <= $iNbrValueArray_1; $jCount++) {
      $iResult += $aArray_2[$iCount - 1] * $aArray_1[$jCount - 1];

      if ($jCount < $iNbrValueArray_1 || $iCount < $iNbrValueArray_2) {
        $sArray .= $aArray_2[$iCount - 1] . " * " . $aArray_1[$jCount - 1] . " + ";
      } else {
        $sArray .= $aArray_2[$iCount - 1] . " * " . $aArray_1[$jCount - 1];
      }
    }
  }

  $sAnswer = "<span style=\"color: grey;\">Le 'Toon' de ces 2 tableaux est donc : <br><br>" .  $sArray . " = " . $iResult . "</span>";
}

require "exo_11.html";

?>