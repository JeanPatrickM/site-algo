<?php

$sAnswer = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
    $sArray = "";

    for ($iCount = 0; $iCount <= 7; $iCount++) {
      if ($iCount === 0 || $iCount === 1) {
        $aSequence[$iCount] = 1;
        $sArray = $sArray . "Index " . $iCount . " : " . $aSequence[$iCount] . "<br>";
      } else {
        $aSequence[$iCount] = $aSequence[$iCount - 1] + $aSequence[$iCount - 2];
        $sArray = $sArray . "Index " . $iCount . " : " . $aSequence[$iCount] . "<br>";
      }
    }

    $sAnswer = "<span style=\"color: grey;\">" . $sArray . "</span>";
}

require "exo_6.html";

?>