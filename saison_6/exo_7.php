<?php

$sAnswer = "";
$sLabel = "";
	
// Réponse de l'exercice
if(isset($_POST['btn_test_php']) && !empty($_POST['btn_test_php'])) {
  $iScore = (int)$_POST['iScore'];
  $iItemScore = (int)$_POST['iItemScore'];
  $iAverage = (int)$_POST['iAverage'];
  $sArray = $_POST['sArray'];

  if ($iItemScore < 9) {
    $aScores[$iItemScore - 1] = $iScore;
    $sArray = $sArray . "Note N°" . $iItemScore . " : " . $aScores[$iItemScore - 1] . "<br>";
    $iAverage += $iScore / 9;
    $sAnswer = "<span style=\"color: grey;\">Note N°" . $iItemScore . " : " . $aScores[$iItemScore - 1] . "<br><br>Poursuivez à côté, merci.</span>";

    $iItemScore++;
    $sLabel = "<span style=\"color: grey;\">Indiquez maintenant la note N°" . $iItemScore . " :</span>";
  } else {
    $aScores[$iItemScore - 1] = $iScore;
    $sArray = $sArray . "Note N°" . $iItemScore . " : " . $aScores[$iItemScore - 1] . "<br>";
    $iAverage += $iScore / 9;
    
    $sLabel = "<span style=\"color: grey;\">Vous avez entré toutes les notes, regardez le récapitulatif à côté.</span>";
    $sAnswer = "<span style=\"color: grey;\">Voici les notes que vous avez indiquées :<br><br>" . $sArray .
                         "<br><br>Et la moyenne de ces notes est : " . $iAverage . "</span>";
  }
} else {
  $iItemScore = 1;
  $iAverage = 0;
  $sArray = "";
}

require "exo_7.html";

?>

<!-- = "<span style=\"color: grey;\"> -->     <!-- </span>" -->